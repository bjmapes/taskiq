# TaskIQ

A Chrome extentions to add common tasks to Jira stories.

## Setup

1. Download TaskIQ.zip from the [Downloads page](https://bitbucket.org/bjmapes/taskiq/downloads/).
1. Unzip to a folder.
1. In Chrome navigate to *chrome:extensions*.
1. Drag the unzipped folder to the Chrome window.
1. Done (It's the blue triangle in the uppder right). 

## License

This project is licensed under the MIT License.
