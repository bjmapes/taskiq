console.log('Content script running.');

chrome.runtime.onMessage.addListener(
    function (subTasksCollection, tabId, sendResponse) {
        (function theLoop(subTasksCollection, i) {
            setTimeout(function () {
                createSubtask(subTasksCollection[i]);
                if (++i < subTasksCollection.length) {                  
                    theLoop(subTasksCollection, i);  
                }
            }, 3000 * i);
        })(subTasksCollection, 0);
    });

function createSubtask(subTask) {
    var SECOND = 2000;

    document.getElementById('create-subtask').click();

    setTimeout(function () {
        fillOutDetails(subTask);
        document.getElementById('create-issue-submit').click();
    }, SECOND);

}

function fillOutDetails(subTask) {
    document.getElementById('summary').value = subTask.summary;
}

function fillOutDetailsJquery(subTask) {
    var SECOND = 2000;

    setTimeout(function () {
        $('#summary').val(subTask.summary);
    }, SECOND);
}