function AuthMinderPlugin() {}

function StoreBase(e) {
    if (e && "inheriting" in e && e.inheriting) return;
    this.availableImpls = [];
    var t = [];
    e && "storageType" in e && e.storageType ? t.push(new e.storageType) : (t.push(new StoreImplPlugin), t.push(new StoreImplLocalStorage), t.push(new StoreImplUserData), t.push(new StoreImplCookies));
    for (var n = 0; n < t.length; n++) {
        var r = t[n];
        typeof r.getType == "function" && typeof r.loadAll == "function" && typeof r.load == "function" && typeof r.remove == "function" && typeof r.save == "function" && this.isAvailable(r) && this.availableImpls.push(r)
    }
    var i = this.availableImpls.length;
    if (i == 0) throw "No store available";
    this.impl = this.availableImpls[0];
    var s = e && "autoMigrate" in e ? e.autoMigrate : !0;
    s && this.migrate()
}

function StoreString(e) {
    StoreBase.call(this, e);
    return
}

function StoreImplMemory() {}

function StoreImplCookies() {}

function StoreImplLocalStorage() {
    try {
        var e = localStorage.length
    } catch (t) {
        aotpLog("StoreLocalImpl: localStorage: " + t)
    }
}

function StoreImplPlugin() {
    this.AMP = null
}

function DeviceLock(e) {
    this.obj = e, this.dlversion = "1"
}

function DeviceLockV2(e) {
    this.obj = e, this.dlversion = "2"
}

function StoreImplUserData() {
    this.maxage = 0, this.userdata = null
}

function ArcotJSClient(e, t) {
    this.lastErrCode = 0, this.lookupMode = 0;
    try {
        t || (t = new DeviceLock(this), t.isValid() ? ArcotLogger.log("V1 devicelocker") : (t = new DeviceLockV2(this), t.isValid() ? ArcotLogger.log("V2 devicelocker") : (t = new DevLockerCurrent(this), t.isValid() ? ArcotLogger.log("Current devicelocker, DL v3 or above") : t = null)));
        var n = {
            autoMigrate: !1,
            devicelocker: t
        };
        this.hdStore = e ? new e(n) : new StoreAID(n)
    } catch (r) {
        ArcotLogger.log(r)
    }
    this.memoryStore = new StoreAIDMemoryStorage, this.aidStore = this.hdStore, this.aidStore.migrate(), this.aidStore.doDevLockMigration(), ArcotLogger.log("Using " + this.aidStore.getType() + " storage")
}

function aotpLog() {
    arguments.length <= 1 ? ArcotLogger.log(arguments[0]) : ArcotLogger.log(arguments[1])
}

function DevLockerCurrent(e) {
    this.obj = e;
    var t = (new AuthMinderPlugin).getPlugin();
    if (t != null) {
        var n = {};
        t.GetDeviceDNA(n), this.dlVersion = parseInt(n.version), ArcotLogger.log("Current device locking version is:" + this.dlVersion)
    } else this.dlVersion = 0
}

function StoreAID(e) {
    this.devicelocker = e && "devicelocker" in e ? e.devicelocker : null, StoreBase.call(this, e), String.prototype.trim || (String.prototype.trim = function() {
        return this.replace(/^\s\s*/, "").replace(/\s\s*$/, "")
    })
}

function StoreAIDMemoryStorage(e) {
    e || (e = {}), e.storageType = StoreImplMemory, e.autoMigrate = !1, StoreAID.call(this, e);
    return
}
AuthMinderPlugin.plugin = null, AuthMinderPlugin.pluginMissing = !1, AuthMinderPlugin.E_SUCCESS = 1e3, AuthMinderPlugin.prototype.getPlugin = function() {
    return AuthMinderPlugin.pluginMissing ? null : (AuthMinderPlugin.plugin == null && this.loadPlugin(), AuthMinderPlugin.plugin != null && ("GetDeviceDNA" in AuthMinderPlugin.plugin || (aotpLog("Reloading the plugin"), this.removePlugin(), this.loadPlugin())), AuthMinderPlugin.plugin)
}, AuthMinderPlugin.prototype.isPluginInstalled = function() {
    try {
        return this.is64BitBrowser() ? window.ActiveXObject ? new ActiveXObject("CA.AuthMinder64") != null : typeof navigator.plugins["CA Technologies AuthMinder64"] != "undefined" : window.ActiveXObject ? new ActiveXObject("CA.AuthMinder") != null : typeof navigator.plugins["CA Technologies AuthMinder"] != "undefined"
    } catch (e) {}
    return !1
}, AuthMinderPlugin.prototype.loadPlugin = function() {
    aotpLog("Attempting to load the plugin");
    if (this.isPluginInstalled()) try {
        if (document.getElementById("arcotjsapiPlugin") == null) {
            var e = document.createElement("div");
            e.setAttribute("id", "arcotjsapiPluginDiv"), e.setAttribute("display", "none"), this.is64BitBrowser() ? e.innerHTML = '<object id="arcotjsapiPlugin" type="application/x-caauthminder-x64" width="0" height="0"></object>' : e.innerHTML = '<object id="arcotjsapiPlugin" type="application/x-caauthminder" width="0" height="0"></object>', document.body.appendChild(e)
        }
        var t = document.getElementById("arcotjsapiPlugin");
        if (t && "GetVersion" in t) {
            var n = t.GetVersion();
            aotpLog("Plugin found, version is: " + n);
            var r = {
                    version: "1",
                    DNA: null
                },
                i = t.GetDeviceDNA(r);
            if (i == AuthMinderPlugin.E_SUCCESS) {
                aotpLog("Plugin loaded and is working. Version: " + t.GetVersion()), aotpLog("DeviceID is: " + r.DNA), AuthMinderPlugin.plugin = t;
                return
            }
            aotpLog("Plugin returned: " + i)
        } else aotpLog("Plugin did not load")
    } catch (s) {
        aotpLog("Exception while loading plugin: " + s)
    } else aotpLog("Plugin is not present");
    aotpLog("Plugin not available"), AuthMinderPlugin.plugin = null, AuthMinderPlugin.pluginMissing = !0
}, AuthMinderPlugin.prototype.removePlugin = function() {
    var e = document.getElementById("arcotjsapiPluginDiv");
    e != null && document.body.removeChild(e), AuthMinderPlugin.plugin = null
}, AuthMinderPlugin.prototype.is64BitBrowser = function() {
    var e = !1;
    if (window.navigator.cpuClass != null && window.navigator.cpuClass.toLowerCase() == "x64" || window.navigator.platform.toLowerCase() == "win64") e = !0;
    return e
}, StoreBase.prototype.getType = function() {
    return this.impl ? this.impl.getType() : null
}, StoreBase.prototype.loadAll = function(e) {
    var t = [],
        n = [],
        r = {},
        i;
    try {
        i = this.impl.loadAll(r);
        if (r.dltus != undefined && r.dltus != null)
            for (var s = 0; s < i; s++) {
                var o = r.values[s],
                    u = r.dltus[s];
                if (o != null) {
                    var a = r.keys[s];
                    if (this.isValidEntry(a, o)) {
                        t.push(a);
                        var f = this.deserialize(o);
                        f.dltu = u, n.push(f)
                    }
                }
            } else
                for (var s = 0; s < i; s++) {
                    var o = r.values[s];
                    if (o != null) {
                        var a = r.keys[s];
                        this.isValidEntry(a, o) && (t.push(a), n.push(this.deserialize(o)))
                    }
                }
    } catch (l) {}
    return e.keys = t, e.values = n, t.length
}, StoreBase.prototype.load = function(e) {
    try {
        var t = this.impl.load(e);
        if (t == null) return null;
        if (t && this.isValidEntry(e, t)) return this.deserialize(t)
    } catch (n) {}
    return null
}, StoreBase.prototype.remove = function(e) {
    try {
        return this.impl.remove(e)
    } catch (t) {}
    return !1
}, StoreBase.prototype.save = function(e, t, n) {
    try {
        return this.impl.save(e, this.serialize(t), n)
    } catch (r) {}
    return !1
}, StoreBase.prototype.serialize = function(e) {
    throw "serialize not overridden"
}, StoreBase.prototype.deserialize = function(e) {
    throw "deserialize not overridden"
}, StoreBase.prototype.isValidEntry = function(e, t) {
    throw "isValidEntry not overridden"
}, StoreBase.prototype.isAvailable = function(e) {
    var t = !1,
        n = "arcottest_" + e.getType(),
        r = (new Date).getTime().toString();
    try {
        e.save(n, r), t = e.load(n) == r, e.remove(n)
    } catch (i) {}
    return t
}, StoreBase.prototype.sbDeviceLock = function() {
    typeof this.deviceLock == "function" && this.deviceLock.apply(this, arguments)
}, StoreBase.prototype.sbDeviceUnlock = function() {
    typeof this.deviceUnlock == "function" && this.deviceUnlock.apply(this, arguments)
}, StoreBase.prototype.migrate = function() {
    var e = this.availableImpls.length;
    aotpLog("migrateKeys: number of stores: " + e);
    if (e < 2) return;
    var t = this.availableImpls[0],
        n = {};
    t.loadAll(n);
    var r = {},
        i = n.keys;
    for (var s = 0; s < i.length; s++) r[i[s]] = 1;
    for (var o = 1; o < this.availableImpls.length; o++) {
        var u = this.availableImpls[o],
            a = {},
            f = u.loadAll(a),
            l = a.keys,
            c = a.values;
        for (var s = 0; s < f; s++) {
            var h = l[s];
            if (h == "arcot.wallet.memory") continue;
            var p = c[s];
            if (p && this.isValidEntry(h, p)) {
                aotpLog("migrateKeys:  key: " + h);
                if (!(h in r)) {
                    var d = {},
                        v = this.deserialize(p);
                    this.sbDeviceUnlock(v, u.getType()), this.sbDeviceLock(v, t.getType()), p = this.serialize(v), t.getType() == "plugin" && v.dltu && (d.inputAttributes = v.dltu), aotpLog("migrateKeys: adding enter to master store: " + h), t.save(h, p, d), r[h] = 1
                }
                aotpLog("migrateKeys: removing entry from lesser store: " + h), u.remove(h)
            }
        }
    }
}, StoreString.prototype = new StoreBase({
    inheriting: !0
}), StoreString.prototype.serialize = function(e) {
    return e
}, StoreString.prototype.deserialize = function(e) {
    return e
}, StoreString.prototype.isValidEntry = function(e, t) {
    return !0
};
var ArcotCookieUtils = {
    loadAll: function(e) {
        var t = [],
            n = [],
            r = document.cookie.split(";");
        for (var i = 0; i < r.length; i++) {
            var s = r[i];
            while (s.charAt(0) == " ") s = s.substring(1, s.length);
            var o = s.split("=");
            o != null && o[1] != null && (t.push(o[0]), n.push(unescape(o[1])))
        }
        return e.keys = t, e.values = n, t.length
    },
    load: function(e) {
        var t = document.cookie.split(";");
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            while (r.charAt(0) == " ") r = r.substring(1, r.length);
            if (r.indexOf(e + "=") == 0) return unescape(r.substring(e.length + 1, r.length))
        }
        return null
    },
    remove: function(e) {
        var t = new Date(0);
        return document.cookie = e + "=;expires=" + t.toGMTString(), !0
    },
    save: function(e, t, n) {
        var r = t ? escape(t) : "";
        if (n) {
            var i = new Date;
            i.setDate(i.getDate() + 1825), document.cookie = e + "=" + r + ";expires=" + i.toGMTString()
        } else document.cookie = e + "=" + r + "; path=/";
        return !0
    }
};
StoreImplMemory.prototype.getType = function() {
    return "memory"
}, StoreImplMemory.prototype.loadAll = function(e) {
    return ArcotCookieUtils.loadAll(e)
}, StoreImplMemory.prototype.load = function(e) {
    return ArcotCookieUtils.load(e)
}, StoreImplMemory.prototype.remove = function(e) {
    return ArcotCookieUtils.remove(e)
}, StoreImplMemory.prototype.save = function(e, t, n) {
    return ArcotCookieUtils.save(e, t, !1)
}, StoreImplCookies.prototype.getType = function() {
    return "cookie"
}, StoreImplCookies.prototype.loadAll = function(e) {
    return ArcotCookieUtils.loadAll(e)
}, StoreImplCookies.prototype.load = function(e) {
    return ArcotCookieUtils.load(e)
}, StoreImplCookies.prototype.remove = function(e) {
    return ArcotCookieUtils.remove(e)
}, StoreImplCookies.prototype.save = function(e, t, n) {
    return ArcotCookieUtils.save(e, t, !0)
}, StoreImplLocalStorage.prototype.getType = function() {
    return "LocalStorage"
}, StoreImplLocalStorage.prototype.loadAll = function(e) {
    if (!this.available()) return null;
    var t = 0,
        n = [],
        r = [],
        i, s;
    while (t < 999999) {
        try {
            i = localStorage.key(t++)
        } catch (o) {
            break
        }
        if (i == null) break;
        s = localStorage.getItem(i), n.push(i), r.push(s)
    }
    return e.keys = n, e.values = r, n.length
}, StoreImplLocalStorage.prototype.load = function(e) {
    return this.available() ? localStorage.getItem(e) : null
}, StoreImplLocalStorage.prototype.remove = function(e) {
    if (!this.available()) return null;
    try {
        localStorage.removeItem(e)
    } catch (t) {
        return !1
    }
    return !0
}, StoreImplLocalStorage.prototype.save = function(e, t, n) {
    if (!this.available()) return null;
    try {
        localStorage.setItem(e, t)
    } catch (r) {
        return !1
    }
    return !0
}, StoreImplLocalStorage.prototype.available = function() {
    try {
        if (typeof localStorage == "undefined") return !1
    } catch (e) {
        return !1
    }
    return !0
}, StoreImplPlugin.DLR = "devlock_required", StoreImplPlugin.DLT = "devlock_type", StoreImplPlugin.DEFAULT_DLTVALUE = "vol_mac_moth_proc_encl", StoreImplPlugin.DLTU = "devlock_typeused", StoreImplPlugin.DLTU_NONE = "none", StoreImplPlugin.PLUGIN_DLT = "inputAttributes", StoreImplPlugin.PLUGIN_DLTU = "outputAttributes", StoreImplPlugin.prototype.getType = function() {
    return "plugin"
}, StoreImplPlugin.prototype.loadAll = function(e) {
    var t = this.getPlugin();
    if (t == null) return null;
    var n = t.GetKeys(),
        r = [],
        i = [];
    if (n != null)
        for (var s = 0; s < n.length; s++) try {
            var o = {
                    key: n[s],
                    value: null
                },
                u = t.Get(o);
            u == AuthMinderPlugin.E_SUCCESS ? (r.push(o.value), i.push(o.outputAttributes)) : (r.push(null), i.push(null))
        } catch (a) {
            r.push(null), i.push(null)
        }
    return e.keys = n, e.values = r, e.dltus = i, n.length
}, StoreImplPlugin.prototype.load = function(e) {
    var t = this.getPlugin();
    if (t == null) return null;
    var n = {
            key: e,
            value: null
        },
        r = t.Get(n);
    return r != AuthMinderPlugin.E_SUCCESS ? null : n.value
}, StoreImplPlugin.prototype.remove = function(e) {
    var t = this.getPlugin();
    if (t == null) return !1;
    var n = t.Delete(e);
    return n == AuthMinderPlugin.E_SUCCESS
}, StoreImplPlugin.prototype.save = function(e, t, n) {
    var r = this.getPlugin();
    if (r == null) return !1;
    var i;
    return n != undefined && n != null ? (n.key = e, n.value = t, i = r.Store(n)) : i = r.Store({
        key: e,
        value: t
    }), i == AuthMinderPlugin.E_SUCCESS
}, StoreImplPlugin.prototype.getPlugin = function() {
    return this.AMP == null && (this.AMP = new AuthMinderPlugin), this.AMP ? this.AMP.getPlugin() : null
}, DeviceLock.prototype.GetVersion = function() {
    return this.dlversion
}, DeviceLock.prototype.isValid = function() {
    try {
        var e = (new AuthMinderPlugin).getPlugin();
        if (e) {
            var t = {},
                n = e.GetDeviceDNA(t);
            if (n == AuthMinderPlugin.E_SUCCESS) {
                var r = t.version;
                if (r == this.dlversion) return !0
            }
        }
    } catch (i) {
        return aotpLog("Exception while getting Plugin Device Lock version: " + i), !1
    }
    return !1
}, DeviceLock.prototype.doDevLockMigration = function(e) {
    return
}, DeviceLock.prototype.getKey = function(t, n, r, i) {
    try {
        var s = (new AuthMinderPlugin).getPlugin();
        if (s) {
            var o = {
                    version: "1",
                    DNA: null
                },
                u = s.GetDeviceDNA(o);
            if (u == AuthMinderPlugin.E_SUCCESS) {
                var a = o.DNA;
                return a
            }
        }
    } catch (f) {}
    return null
}, DeviceLockV2.prototype.GetVersion = function() {
    return this.dlversion
}, DeviceLockV2.prototype.isValid = function() {
    try {
        var t = (new AuthMinderPlugin).getPlugin();
        if (t) {
            var n = {},
                r = t.GetDeviceDNA(n);
            if (r == AuthMinderPlugin.E_SUCCESS) {
                var i = n.version;
                if (i == this.dlversion) return !0
            }
        }
    } catch (s) {
        return aotpLog("Exception while getting Plugin Device Lock version: " + s), !1
    }
    return !1
}, DeviceLockV2.prototype.doDevLockMigration = function(e) {
    e.doDevLockMigrationV2()
}, DeviceLockV2.prototype.getKey = function(t, n, r, i) {
    try {
        var s = (new AuthMinderPlugin).getPlugin();
        if (!s) return null;
        if (!(!t || t.toUpperCase() != "FALSE" && t.toUpperCase() != "NO")) return i.dltu = StoreImplPlugin.DLTU_NONE, null;
        var o = {
            version: "2",
            DNA: null
        };
        if (r) n != undefined && n != null ? o.inputAttributes = n : o.inputAttributes = "";
        else {
            if (n == undefined || n == null) return null;
            o.inputAttributes = n
        }
        var u = s.GetDeviceDNA(o);
        if (u == AuthMinderPlugin.E_SUCCESS) {
            var a = o.DNA;
            return i.dltu = o.outputAttributes, a
        }
    } catch (f) {}
    return null
}, StoreImplUserData.prototype.getType = function() {
    return "userData"
}, StoreImplUserData.prototype.loadAll = function(e) {
    if (!this.checkUserDataLoaded()) return 0;
    var t = [],
        n = [];
    try {
        var r = this.userdata.xmlDocument.firstChild.attributes,
            i = r.length;
        for (var s = 0; s < i; s++) {
            var o = r[s];
            t.push(o.nodeName), n.push(o.nodeValue)
        }
    } catch (u) {
        aotpLog("Exception in userData loadall: " + u)
    }
    return e.keys = t, e.values = n, t.length
}, StoreImplUserData.prototype.load = function(e) {
    return this.checkUserDataLoaded() ? this.userdata.getAttribute(e) : null
}, StoreImplUserData.prototype.remove = function(e) {
    return this.checkUserDataLoaded() ? (this.userdata.removeAttribute(e), this.userdata.save("ArcotUserDataStorage"), !0) : !1
}, StoreImplUserData.prototype.save = function(e, t, n) {
    return this.checkUserDataLoaded() ? (this.userdata.setAttribute(e, t), this.userdata.save("ArcotUserDataStorage"), !0) : !1
}, StoreImplUserData.prototype.checkUserDataLoaded = function() {
    return this.userdata == null && this.loadUserData(), this.userdata ? !0 : !1
}, StoreImplUserData.prototype.loadUserData = function() {
    aotpLog("Attempting to load the UserData");
    try {
        this.userdata = document.createElement("div"), this.userdata.setAttribute("id", "arcotuserdataDiv"), this.userdata.setAttribute("display", "none"), this.userdata.style.display = "none", this.userdata.style.behavior = "url('#default#userData')", document.body.appendChild(this.userdata);
        if (this.maxage) {
            var e = (new Date).getTime(),
                t = e + this.maxage * 1e3;
            this.userdata.expires = (new Date(t)).toUTCString()
        }
        this.userdata.load("ArcotUserDataStorage")
    } catch (n) {
        aotpLog("Exception while loading userData: " + n), this.userdata = null
    }
}, StoreImplUserData.prototype.removeUserData = function() {
    this.userdata != null && document.body.removeChild(this.userdata), this.userdata = null
};
var ArcotErrorCodes = {
    ERR_NONE: 0,
    ERR_BAD_PIN: 1,
    ERR_MISSING_WALLET: 3,
    ERR_BAD_WALLET: 6,
    ERR_ILLEGAL_CHALLENGE: 17,
    ERR_UNKNOWN: 18,
    ERR_ILLEGAL_DOMAIN_ACCESS: 20
};
ArcotJSClient.prototype.GetVersion = function() {
    return "6.0.4.7"
}, ArcotJSClient.prototype.GetVersionEx = function() {
    return "ArcotID Javascript Client " + this.GetVersion()
}, ArcotJSClient.prototype.GetErrorCode = function() {
    return this.lastErrCode
}, ArcotJSClient.prototype.GetErrorMessage = function() {
    switch (this.lastErrCode) {
        case ArcotErrorCodes.ERR_NONE:
            return "No error";
        case ArcotErrorCodes.ERR_BAD_PIN:
            return "Entered PIN was invalid";
        case ArcotErrorCodes.ERR_MISSING_WALLET:
            return "The user does not have an Arcot wallet";
        case ArcotErrorCodes.ERR_BAD_WALLET:
            return "Invalid wallet";
        case ArcotErrorCodes.ERR_ILLEGAL_CHALLENGE:
            return "Missing or illegal authentication challenge";
        case ArcotErrorCodes.ERR_UNKNOWN:
            return "Unknown error";
        case ArcotErrorCodes.ERR_ILLEGAL_DOMAIN_ACCESS:
            return "Arcot ID accessed from an unauthorized domain";
        default:
            return "Uncatalogued error"
    }
}, ArcotJSClient.prototype.SetAttribute = function(e, t) {
    var n = !1;
    return e != null && (e = e.toLowerCase(), e == "aid_lookup_mode" && t != null && (t = t.toLowerCase(), t == "alias_and_username" && (this.lookupMode = 0, n = !0), t == "username_only" && (this.lookupMode = 1, n = !0), t == "alias_only" && (this.lookupMode = 2, n = !0)), e == "storagetype" && this.UpdateStore(t)), n
}, ArcotJSClient.prototype.GetGlobalAttribute = function(e) {
    this.lastErrCode = ArcotErrorCodes.ERR_NONE, ArcotGlobalAttrib.parse(e);
    var t = null;
    switch (ArcotGlobalAttrib.type) {
        case ArcotGlobalAttrib.NONE:
            e == "" && (t = "");
            break;
        case ArcotGlobalAttrib.WLT_CNT:
            t = "" + this.aidStore.GetWalletCount();
            break;
        case ArcotGlobalAttrib.WLT_AID:
            t = null;
            break;
        case ArcotGlobalAttrib.WLT_NAM:
            var n = this.aidStore.GetWalletAt(ArcotGlobalAttrib.walletPos);
            t = "" + n.walletname;
            break;
        case ArcotGlobalAttrib.WLT_ORG:
            var n = this.aidStore.GetWalletAt(ArcotGlobalAttrib.walletPos);
            t = "" + n.arcotcards[0].orgname;
            break;
        case ArcotGlobalAttrib.CRD_CNT:
            t = null;
            break;
        case ArcotGlobalAttrib.CRD_NAM:
            t = null;
            break;
        case ArcotGlobalAttrib.CRD_SNO:
            t = null;
            break;
        case ArcotGlobalAttrib.CRD_DER:
            t = null;
            break;
        case ArcotGlobalAttrib.ALIAS_CNT:
            var r = ArcotGlobalAttrib.userNameOrAlias,
                i = ArcotGlobalAttrib.appctxt,
                s = ArcotGlobalAttrib.org,
                o = this.GetWallet(r, s, i);
            t = o == null ? 0 : 1;
            break;
        default:
            t = null
    }
    return t
}, ArcotJSClient.prototype.ImportArcotID = function(e, t, n) {
    this.lastErrCode = ArcotErrorCodes.ERR_NONE;
    if (e == null || e.length == 0) return this.lastErrCode = ArcotErrorCodes.ERR_BAD_WALLET, !1;
    this.UpdateStore(t);
    var r = this.aidStore.deserialize(e);
    this.aidStore.deviceLock(r, this.aidStore.getType());
    var i = this.aidStore.Save(r, n);
    return i != ArcotErrorCodes.ERR_NONE ? (ArcotLogger.log("Error code is:" + i), this.lastErrCode = i, !1) : !0
}, ArcotJSClient.prototype.RemoveArcotID = function(e, t, n) {
    this.lastErrCode = ArcotErrorCodes.ERR_NONE, this.UpdateStore(n);
    var r = this.aidStore.Delete(e, t);
    return r || (this.lastErrCode = ArcotErrorCodes.ERR_MISSING_WALLET), r
}, ArcotJSClient.prototype.RemoveArcotIDEx = function(e, t, n, r) {
    this.lastErrCode = ArcotErrorCodes.ERR_NONE, this.UpdateStore(r);
    var i = !1;
    switch (this.lookupMode) {
        case 0:
            i = this.aidStore.Delete(e, n);
            if (i) break;
            i = this.aidStore.DeleteByAlias(e, n, t);
            break;
        case 1:
            i = this.aidStore.Delete(e, n);
            break;
        case 2:
            i = this.aidStore.DeleteByAlias(e, n, t)
    }
    return i || (this.lastErrCode = ArcotErrorCodes.ERR_MISSING_WALLET), i
}, ArcotJSClient.prototype.SignChallengeEx = function(e, t, n, r, i) {
    this.lastErrCode = ArcotErrorCodes.ERR_NONE;
    var s = this.aidStore.Get(t, r);
    return s == null ? (this.lastErrCode = ArcotErrorCodes.ERR_MISSING_WALLET, null) : this.SignChallengeWithWallet(e, s, i)
}, ArcotJSClient.prototype.SignChallengeEx2 = function(e, t, n, r, i) {
    this.lastErrCode = ArcotErrorCodes.ERR_NONE;
    var s = this.GetWallet(n, i, r);
    return s == null ? (this.lastErrCode = ArcotErrorCodes.ERR_MISSING_WALLET, null) : this.SignChallengeWithWallet(e, s, t)
}, ArcotJSClient.prototype.SignChallengeWithWallet = function(e, t, n) {
    if (e == null || e.length == 0) return this.lastErrCode = ArcotErrorCodes.ERR_ILLEGAL_CHALLENGE, null;
    var r = new Date,
        i = new ArcotAIDMobile.ArcotDigestRep(t.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageType, t.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.salt, t.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.iterationCount, t.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.checksum, t.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.start, t.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.span);
    this.aidStore.deviceUnlock(this, this.aidStore.getType(), n, t) || (this.decamkey = i.decrypt(n, ArcotAIDMobile.hexstr2bytes(t.arcotcards[0].arcotPrivateKey.camouflagedKey))), e = e.replace(/_/g, "+");
    var s = ArcotBase64.b64toBA(e),
        o = new Array;
    o[0] = 4, o[1] = s.length;
    for (var u = 0; u < s.length; u++) o[u + 2] = s[u];
    var a = ArcotAIDMobile.getBytesToSign(o),
        f = new ArcotJSBN.BigInteger(this.decamkey.rsakey.modulus, 16),
        l = new ArcotJSBN.BigInteger(this.decamkey.rsakey.exponent, 16),
        c = ArcotAIDMobile.arcotsign(f, l, a);
    c.length % 2 != 0 && (c = "0" + c), c = "00" + c;
    var h = ArcotAIDMobile.hexstr2bytes(c),
        p = new Array;
    p[0] = 3;
    var d = ArcotAIDMobile.getLengthBytes(h.length);
    for (var u = 0; u < d.length; u++) p[p.length] = d[u];
    for (var u = 0; u < h.length; u++) p[p.length] = h[u];
    var v = new Array(48, 14, 6, 10, 96, 134, 72, 1, 134, 249, 70, 9, 2, 0, 5, 0),
        m = o.length + v.length + p.length,
        g = new Array;
    g[0] = 48;
    var y = ArcotAIDMobile.getLengthBytes(m);
    for (var u = 0; u < y.length; u++) g[g.length] = y[u];
    for (var u = 0; u < o.length; u++) g[g.length] = o[u];
    for (var u = 0; u < v.length; u++) g[g.length] = v[u];
    for (var u = 0; u < p.length; u++) g[g.length] = p[u];
    var b = ArcotAIDMobile.hexstr2bytes(t.arcotcards[0].certificate.rawdata),
        w = new Array;
    w[0] = 48;
    var E = ArcotAIDMobile.getLengthBytes(b.length);
    for (var u = 0; u < E.length; u++) w[w.length] = E[u];
    for (var u = 0; u < b.length; u++) w[w.length] = b[u];
    var S = new Array;
    S[0] = 48;
    var x = 3 + w.length + g.length,
        T = ArcotAIDMobile.getLengthBytes(x);
    for (var u = 0; u < T.length; u++) S[S.length] = T[u];
    S[S.length] = 2, S[S.length] = 1, S[S.length] = 0;
    for (var u = 0; u < w.length; u++) S[S.length] = w[u];
    for (var u = 0; u < g.length; u++) S[S.length] = g[u];
    var N = ArcotAIDMobile.bytear2hexstr(S),
        C = ArcotBase64.hex2b64(N);
    C = C.replace(/\+/g, "_");
    var k = new Date;
    return ArcotLogger.log("time to sign: " + (k.getTime() - r.getTime())), C
}, ArcotJSClient.prototype.GetWallet = function(e, t, n) {
    var r = null;
    switch (this.lookupMode) {
        case 0:
            r = this.aidStore.Get(e, t);
            if (r != null) break;
            r = this.GetWalletByAlias(e, t, n);
            break;
        case 1:
            r = this.aidStore.Get(e, t);
            break;
        case 2:
            r = this.GetWalletByAlias(e, t, n)
    }
    return r
}, ArcotJSClient.prototype.GetWalletByAlias = function(e, t, n) {
    var r = null,
        i = this.aidStore.GetAll();
    for (var s = 0; s < i.length; s++) {
        var o = i[s].b64EncodedArcotID,
            u = WalletUtil.parse(o);
        u.dltu = i[s].dltu;
        var a = "Alias." + n,
            f = u.arcotcards[0].namedAttributes[a];
        f != null && (f = ArcotUtil.util_hex2str(f));
        if (f != null && f.toUpperCase() == e.toUpperCase()) {
            if (t == null || t.length == 0) {
                r = u;
                break
            }
            t.toUpperCase() == u.arcotcards[0].orgname.toUpperCase() && (r = u)
        }
    }
    return r
}, ArcotJSClient.prototype.GetWalletIndexByAlias = function(e, t, n) {
    var r = -1,
        i = this.aidStore.GetAll();
    for (var s = 0; s < i.length; s++) {
        var o = i[s].b64EncodedArcotID,
            u = WalletUtil.parse(o),
            a = "Alias." + n,
            f = u.arcotcards[0].namedAttributes[a];
        f != null && (f = ArcotUtil.util_hex2str(f));
        if (f != null && f.toUpperCase() == e.toUpperCase()) {
            if (t == null || t.length == 0) {
                r = s;
                break
            }
            t.toUpperCase() == u.arcotcards[0].orgname.toUpperCase() && (r = s)
        }
    }
    return r
}, ArcotJSClient.prototype.UpdateStore = function(e) {
    e != null && (e = e.toLowerCase(), e == "hd" && (this.aidStore = this.hdStore), e == "memory" && (this.aidStore = this.memoryStore))
};
var ArcotGlobalAttrib = new function() {
        this.NONE = 0, this.WLT_CNT = 1, this.WLT_AID = 2, this.WLT_NAM = 3, this.WLT_ORG = 4, this.CRD_CNT = 5, this.CRD_NAM = 6, this.CRD_SNO = 7, this.CRD_DER = 8, this.ALIAS_CNT = 9, this.type = this.NONE, this.walletPos = 0, this.cardPos = 0, this.userNameOrAlias = null, this.appctxt = null, this.org = null;
        var e = /^\d+$/;
        this.parse = function(t) {
            this.type = this.NONE, this.walletPos = 0, this.cardPos = 0, this.userNameOrAlias = null, this.appctxt = null, this.org = null;
            var n = ".";
            t.length > 7 && t.substr(0, 7).toLowerCase() == "walletn" ? n = t.charAt(7) : t.length > 6 && t.substr(0, 6).toLowerCase() == "wallet" ? n = t.charAt(6) : t.length > 5 && t.substr(0, 5).toLowerCase() == "alias" && (n = t.charAt(5));
            var r = t.split(n);
            if (r.length == 0) return;
            var i = 0,
                s = r[i];
            s = s.toLowerCase();
            if (s == "walletn") {
                var o = r[++i];
                o != null && (o = o.toLowerCase());
                if (o != null)
                    if (o == "count") this.type = this.WLT_CNT;
                    else if (e.test(o)) {
                    this.walletPos = parseInt(o);
                    var u = r[++i];
                    u != null && (u = u.toLowerCase());
                    if (u == null) this.type = this.WLT_AID;
                    else if (u == "name") this.type = this.WLT_NAM;
                    else if (u == "org") this.type = this.WLT_ORG;
                    else if (u == "cardn") {
                        var f = r[++i];
                        f != null && (f = f.toLowerCase());
                        if (f != null)
                            if (f == "count") this.type = this.CRD_CNT;
                            else if (e.test(f)) {
                            this.cardPos = parseInt(f);
                            var l = r[++i];
                            l != null && (l = l.toLowerCase()), l != null && (l == "name" ? this.type = this.CRD_NAM : l == "serialnumber" && (this.type = this.CRD_SNO))
                        }
                    }
                }
            }
            if (s == "alias") {
                this.type = this.ALIAS_CNT;
                if (r.length == 5 || r.length == 7) this.userNameOrAlias = r[1], this.appctxt = r[3], r.length == 7 && (this.org = r[5])
            }
        }
    },
    ArcotAIDMobile = {
        debug: !1,
        randindex: 0,
        fixedRandom: function(e, t) {
            var n;
            for (n = 0; n < t; n++) e[n] = ArcotAIDMobile.randindex++ & 255;
            return e
        },
        str2bytes: function(e) {
            var t = new Array,
                n = 255;
            for (var r = 0; r < e.length; r++) {
                var i = e.charCodeAt(r) & n;
                t[r] = i
            }
            return t
        },
        hexstr2bytes: function(e) {
            var t = new Array;
            e.length % 2 != 0 && (e = "0" + e);
            var n = e.length / 2 & 4294967295,
                r = 0;
            for (var i = 0; i < n; i++) {
                var s = parseInt(e.substring(r, r + 2), 16);
                t[i] = s, r += 2
            }
            return t
        },
        arraycopy: function(e, t, n, r, i) {
            for (var s = 0; s < i; s++) n[s + r] = e[s + t];
            return n
        },
        arraytostr: function(e) {
            var t = "";
            for (var n = 0; n < e.length; n++) t += e[n] + " ";
            return t
        },
        arraytohexstr: function(e) {
            var t = "";
            for (var n = 0; n < e.length; n++) t += e[n].toString(16) + " ";
            return t
        },
        intar2bytear: function(e) {
            var t = new Array,
                n = 0;
            for (var r = 0; r < e.length; r++) t[n++] = (e[r] & 4278190080) >> 24 & 255, t[n++] = (e[r] & 16711680) >> 16 & 255, t[n++] = (e[r] & 65280) >> 8 & 255, t[n++] = e[r] & 255 & 255;
            return t
        },
        bytear2intar: function(e) {
            var t = new Array,
                n = 0;
            for (var r = 0; r < e.length;) {
                var i = 0;
                i |= e[r++] << 24, i |= e[r++] << 16, i |= e[r++] << 8, i |= e[r++], t[n++] = i
            }
            return t
        },
        bytear2hexstr: function(e) {
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = ArcotUtil.util_byte2hex(e[n]);
                r.length < 2 && (r = "0" + r), t += r
            }
            return t
        },
        strToBytes: function(e) {
            var t = new Array;
            for (var n = 0; n < e.length; n++) t[n] = e.charCodeAt(n);
            return t
        },
        alertlong: function(e) {
            var t = e.split(/\r?\n/),
                n = 40,
                r = 0,
                i = "";
            for (var s = 0; s < t.length; s++) s > 0 && s % n == 0 ? (ArcotAIDMobile.debug && alert(i), i = t[s]) : i += "\n" + t[s];
            ArcotAIDMobile.debug && alert(i)
        },
        calculate: function(e, t) {
            form = document.forms.calcform, e = form.elements.bignum1.value, t = form.elements.bignum2.value, bignum3 = form.elements.bignum3.value;
            var n = new ArcotJSBN.BigInteger(e, 10),
                r = new ArcotJSBN.BigInteger(t, 10),
                i = new ArcotJSBN.BigInteger(bignum3, 10),
                s = ArcotAIDMobile.enddn.getMinutes() * 60 + ArcotAIDMobile.enddn.getSeconds() - (startdn.getMinutes() * 60 + startdn.getSeconds());
            form.elements.bn1bits.value = n.bitLength(), form.elements.bn2bits.value = r.bitLength(), form.elements.bn3bits.value = i.bitLength();
            var o = n.add(r);
            form.elements.sum.value = o;
            var u = n.subtract(r);
            form.elements.diff.value = u;
            var a = n.multiply(r);
            form.elements.prod.value = a;
            var f = new Date,
                l = n.modPow(r, i);
            form.elements.modexp.value = l;
            var c = new Date,
                h = c.getMinutes() * 60 + c.getSeconds() - (f.getMinutes() * 60 + f.getSeconds());
            return form.elements.calctime.value = h, form.elements.dntime.value = s, !0
        },
        ArcotWallet: function() {
            this.counter = 0, this.walletversion, this.walletname, this.arcotcards = new Array, this.handlerobject = this
        },
        ArcotCard: function(e) {
            this.parent = e, this.version, this.cardname, this.orgname, this.lastattr = null, this.counter = 0, this.seqcount = 0, this.arcotPrivateKey, this.plainPrivateKey, this.certificate, this.namedAttributes, this.handlerobject, this.dummyattribute
        },
        ArcotProtectedKey: function(e) {
            this.parent = e, this.arcotDigest, this.camouflagedKey, this.camkeyoffset, this.camkeylength, this.counter = 0, this.handlerobject
        },
        ArcotDigest: function(e) {
            this.handlerobject, this.camouflageType, this.camouflageParameters, this.parent = e, this.counter = 0
        },
        CamouflageParameters: function(e) {
            this.parent = e, this.handlerobject, this.salt, this.iterationCount, this.checksum, this.span, this.start, this.counter = 0
        },
        Name: function(e) {
            this.parent = e, this.counter = 0, this.handlerobject, this.hsParameters = new Array, this.seqcount = 0, this.lastoid
        },
        nameHandleTag: function(e, t) {
            if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
                this.handlerobject.handleTag(e, t);
                return
            }
            e == "SEQUENCE" ? this.seqcount++ : e == "ENDSEQUENCE" ? (this.seqcount--, this.seqcount < 0 && this.parent.notifyEnd()) : e == "OBJECTIDENTIFIER" ? this.lastoid = t : e == "PrintableString" | e == "IA5String" && this.lastoid != null && (this.hsParameters[this.lastoid] = t, this.lastoid = null)
        },
        Certificate: function(e) {
            this.parent = e, this.counter = 0, this.handlerobject, this.version, this.serialnum, this.algorithmid, this.issuername, this.notbefore, this.notafter, this.subjectname, this.subjectkeytype, this.subjectpublickey, this.signatureAlgorithm, this.signature, this.rawdata, this.extseqcount = 0
        },
        certHandleTag: function(e, t) {
            if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
                this.handlerobject.handleTag(e, t);
                return
            }
            e == "INTEGER" && this.counter == 0 ? (this.version = t, this.counter++) : e == "INTEGER" && this.counter == 1 ? (this.serialnum = t, this.counter++) : e == "OBJECTIDENTIFIER" && this.counter == 2 ? (this.algorithmid = t, this.counter++) : e == "SEQUENCE" && this.counter == 3 ? (this.issuername = new ArcotAIDMobile.Name(this), this.handlerobject = this.issuername, this.counter++) : e == "UTCTime" && this.counter == 4 ? (this.notbefore = t, this.counter++) : e == "UTCTime" && this.counter == 5 ? (this.notafter = t, this.counter++) : e == "SEQUENCE" && this.counter == 6 ? (this.subjectname = new ArcotAIDMobile.Name(this), this.handlerobject = this.subjectname, this.counter++) : e == "OBJECTIDENTIFIER" && this.counter == 7 ? (this.subjectkeytype = t, this.counter++) : e == "BITSTRING" && this.counter == 8 ? (this.subjectpublickey = t, this.counter++) : e != "CONTEXTSPECIFIC" && (e == "SEQUENCE" && this.counter == 9 ? this.extseqcount++ : e == "ENDSEQUENCE" && this.counter == 9 ? (this.extseqcount--, this.extseqcount <= 0 && this.counter++) : e == "OBJECTIDENTIFIER" && this.counter == 10 ? (this.signatureAlgorithm = t, this.counter++) : e == "BITSTRING" && this.counter == 11 ? (this.signature = t, this.counter++) : e == "ENDSEQUENCE" && this.counter == 12 ? this.parent.notifyEnd() : e == "BITSTRING" && this.counter == 12)
        },
        cparamsHandleTag: function(e, t) {
            if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
                this.handlerobject.handleTag(e, t);
                return
            }
            e == "INTEGER" ? this.counter == 1 ? (this.iterationCount = t, this.counter++) : this.counter == 2 ? (this.checksum = t, this.counter++) : this.counter == 3 ? (this.span = t, this.counter++) : this.counter == 4 && (this.start = t, this.counter++) : e == "OCTETSTRING" ? this.counter == 0 && (this.salt = t, this.counter++) : e == "BITSTRING" ? this.counter == 2 && (this.checksum = t, this.counter++) : e != "SEQUENCE" && e == "ENDSEQUENCE" && this.parent.notifyEnd()
        },
        ardHandleTag: function(e, t) {
            if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
                this.handlerobject.handleTag(e, t);
                return
            }
            e == "OBJECTIDENTIFIER" ? this.camouflageType = t : e == "SEQUENCE" ? this.counter == 0 && (this.camouflageParameters = new ArcotAIDMobile.CamouflageParameters(this), this.handlerobject = this.camouflageParameters) : e == "ENDSEQUENCE" && this.parent.notifyEnd()
        },
        apkHandleTag: function(e, t, n, r) {
            if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
                this.handlerobject.handleTag(e, t);
                return
            }
            e == "SEQUENCE" ? this.counter == 0 ? this.counter++ : this.counter == 1 && (this.arcotDigest = new ArcotAIDMobile.ArcotDigest(this), this.handlerobject = this.arcotDigest) : e == "ENDSEQUENCE" ? this.parent.notifyEnd() : this.counter == 2 && e == "OCTETSTRING" && (this.camouflagedKey = t, this.camkeyoffset = r, this.camkeylength = n)
        },
        apkNotifyEnd: function() {
            this.handlerobject = null, this.counter++
        },
        ardNotifyEnd: function() {
            this.handlerobject = null, this.counter++;
            var e = "digest details:\n";
            e += "camouflageType: " + this.camouflageType + "\n", e += "salt: " + this.camouflageParameters.salt + "\n", e += "iterationCount: " + this.camouflageParameters.iterationCount + "\n", e += "checksum: " + this.camouflageParameters.checksum + "\n", e += "span: " + this.camouflageParameters.span + "\n", e += "start: " + this.camouflageParameters.start
        },
        certNotifyEnd: function() {
            this.handlerobject = null
        },
        cparamsNotifyEnd: function() {
            this.handlerobject = null
        },
        arwNotifyEnd: function() {
            this.handlerobject = null
        },
        arcNotifyEnd: function() {
            this.handlerobject = null
        },
        nameNotifyEnd: function() {
            this.handlerobject = null
        },
        arcHandleTag: function(e, t, n, r) {
            if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
                this.handlerobject.handleTag(e, t, n, r);
                return
            }
            if (this.counter == 0 && e == "INTEGER") this.version = t, this.counter++;
            else if (this.counter == 1 && e == "UTF8String") this.cardname = t, this.counter++;
            else if (e == "SEQUENCE") this.seqcount++;
            else if (e == "ENDSEQUENCE") this.seqcount == 0 ? this.parent.notifyEnd() : this.seqcount--;
            else if (e == "CONTEXTSPECIFIC") {
                if (t == 0) {
                    var i = new ArcotAIDMobile.ArcotProtectedKey(this);
                    this.arcotPrivateKey = i, this.handlerobject = i
                } else if (t != 1)
                    if (t == 2) {
                        var s = new ArcotAIDMobile.Certificate(this);
                        s.rawdata = ArcotASN1JS.lastcontextvalue, this.certificate = s, this.handlerobject = s
                    } else t == 3 && (this.namedAttributes = new Array, this.counter = 5)
            } else this.counter == 5 && e == "IA5String" ? this.lastattr = t : this.counter == 5 && e == "OCTETSTRING" ? this.lastattr != null && (this.lastattr == "Org" && (this.orgname = ArcotUtil.util_hex2tstring(t)), this.namedAttributes[this.lastattr] = t, this.lastattr = null) : e == "OBJECTIDENTIFIER"
        },
        arwAddTag: function(e, t, n, r, i) {
            this.handlerobject != null && this.handlerobject.handleTag(e, t, r, i)
        },
        arwHandleTag: function(e, t) {
            this.counter == 0 && e == "INTEGER" ? (this.walletversion = t, this.counter++) : this.counter == 1 && e == "UTF8String" ? (this.walletname = t, this.counter++) : this.counter == 2 && e == "SEQUENCE" ? this.counter++ : this.counter == 3 && (e == "SEQUENCE" ? (this.arcotcards[this.arcotcards.length] = new ArcotAIDMobile.ArcotCard(this), this.handlerobject = this.arcotcards[this.arcotcards.length - 1]) : e == "ENDSEQUENCE" && this.counter++)
        }
    };
ArcotAIDMobile.ArcotWallet.prototype.addTag = ArcotAIDMobile.arwAddTag, ArcotAIDMobile.ArcotWallet.prototype.notifyEnd = ArcotAIDMobile.arwNotifyEnd, ArcotAIDMobile.ArcotCard.prototype.notifyEnd = ArcotAIDMobile.arcNotifyEnd, ArcotAIDMobile.ArcotProtectedKey.prototype.notifyEnd = ArcotAIDMobile.apkNotifyEnd, ArcotAIDMobile.ArcotDigest.prototype.notifyEnd = ArcotAIDMobile.ardNotifyEnd, ArcotAIDMobile.CamouflageParameters.prototype.notifyEnd = ArcotAIDMobile.cparamsNotifyEnd, ArcotAIDMobile.Certificate.prototype.notifyEnd = ArcotAIDMobile.certNotifyEnd, ArcotAIDMobile.Name.prototype.notifyEnd = ArcotAIDMobile.nameNotifyEnd, ArcotAIDMobile.ArcotWallet.prototype.handleTag = ArcotAIDMobile.arwHandleTag, ArcotAIDMobile.ArcotCard.prototype.handleTag = ArcotAIDMobile.arcHandleTag, ArcotAIDMobile.ArcotProtectedKey.prototype.handleTag = ArcotAIDMobile.apkHandleTag, ArcotAIDMobile.ArcotDigest.prototype.handleTag = ArcotAIDMobile.ardHandleTag, ArcotAIDMobile.CamouflageParameters.prototype.handleTag = ArcotAIDMobile.cparamsHandleTag, ArcotAIDMobile.Certificate.prototype.handleTag = ArcotAIDMobile.certHandleTag, ArcotAIDMobile.Name.prototype.handleTag = ArcotAIDMobile.nameHandleTag, ArcotAIDMobile.enddn = new Date, ArcotAIDMobile.getCryptoType = function(e) {
    return e == "2.16.840.1.113862.9.1.1" ? 0 : -1
}, ArcotAIDMobile.ArcotDigestRep = function(e, t, n, r, i, s) {
    this.oid = e, this.salt = t, this.iterations = n, this.phenc = r, this.start = i + 0, this.span = s + 0, this.cryptotype = ArcotAIDMobile.getCryptoType(this.oid)
}, ArcotAIDMobile.xorBits = function(e, t, n, r, i, s) {
    var o = 8;
    t[0] &= 255 >> i % o;
    var u = ~(255 >> (i + s) % o) & 255 & 255;
    u != 0 && (t[r - 1] &= u);
    for (var a = 0; a < r; ++a) e[n + a] ^= t[a]
}, ArcotAIDMobile.RSAKeyWrapper = function() {
    this.oid, this.octetstring, this.rsakey, this.count = 0, this.handlerobject
}, ArcotAIDMobile.RSAKey = function(e) {
    this.parent = e, this.exponent, this.modulus, this.count = 0, this.octetstring, this.handlerobject
}, ArcotAIDMobile.rsakeywrapHandleTag = function(e, t) {
    if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
        this.handlerobject.handleTag(e, t);
        return
    }
    e == "OBJECTIDENTIFIER" && this.count == 0 ? (this.oid = t, this.count++) : e == "OCTETSTRING" && this.count == 1 && (this.octetstring = t, this.octetstring = t, this.rsakey = new ArcotAIDMobile.RSAKey, this.rsakey.octetstring = this.octetstring, this.rsakey.parseKey(), this.count++)
}, ArcotAIDMobile.rsakeyParseKey = function() {
    var e = ArcotUtil.util_hex2bytearray(this.octetstring),
        t = 128,
        n = 1;
    (e[n] & t) == t && (n += e[n] & t - 1), n++, n++;
    var r = 0;
    if ((e[n] & t) == t) {
        var i = e[n] & t - 1;
        for (var s = 0; s < i; s++) n++, r = r << 8 | e[n];
        n++
    } else r = e[n++];
    this.modulus = this.octetstring.substring(n * 2, n * 2 + r * 2), n += r;
    if (e[n] != 2) return;
    n++;
    var o = 0;
    if ((e[n] & t) == t) {
        var i = e[n] & t - 1;
        for (var s = 0; s < i; s++) n++, o = o << 8 | e[n];
        n++
    } else o = e[n++];
    this.exponent = this.octetstring.substring(n * 2, n * 2 + o * 2)
}, ArcotAIDMobile.rsakeywrapAddTag = function(e, t, n) {
    this.handleTag(e, t)
}, ArcotAIDMobile.rsakeyHandleTag = function(e, t) {
    if (this.handlerobject != null && this.handlerobject.handleTag != undefined) {
        this.handlerobject.handleTag(e, t);
        return
    }
    e == "INTEGER" && this.count == 0 ? (this.modulus = t, this.count++) : e == "INTEGER" && this.count == 1 ? (this.exponent = t, this.count++) : e == "ENDSEQUENCE" && this.count == 1 && (this.exponent = t, this.count++)
}, ArcotAIDMobile.rsakeyAddTag = function(e, t, n) {
    this.handleTag(e, t)
}, ArcotAIDMobile.RSAKeyWrapper.prototype.handleTag = ArcotAIDMobile.rsakeywrapHandleTag, ArcotAIDMobile.RSAKeyWrapper.prototype.addTag = ArcotAIDMobile.rsakeywrapAddTag, ArcotAIDMobile.RSAKey.prototype.parseKey = ArcotAIDMobile.rsakeyParseKey, ArcotAIDMobile.RSAKey.prototype.handleTag = ArcotAIDMobile.rsakeyHandleTag, ArcotAIDMobile.RSAKey.prototype.addTag = ArcotAIDMobile.rsakeyAddTag, ArcotAIDMobile.applyPinToKey = function(e, t) {
    if (this.cryptotype == 0) {
        var n = this.start / 8 & 4294967295,
            r = (this.start + this.span + 7) / 8 - n;
        e = unescape(encodeURIComponent(e));
        var i = ArcotCrypto.crypto_mgf1(e, r),
            s = ArcotUtil.util_tstring2hex(i),
            o = ArcotAIDMobile.hexstr2bytes(s),
            u = t;
        return ArcotAIDMobile.xorBits(u, o, n, r, this.start, this.span), u
    }
}, ArcotAIDMobile.adrDecrypt = function(e, t) {
    if (this.cryptotype == 0) {
        var n = this.applyPinToKey(e, t),
            r = new ArcotAIDMobile.RSAKeyWrapper;
        return ArcotASN1JS.decode(ArcotAIDMobile.bytear2hexstr(n), r), r
    }
}, ArcotAIDMobile.ArcotDigestRep.prototype.decrypt = ArcotAIDMobile.adrDecrypt, ArcotAIDMobile.ArcotDigestRep.prototype.applyPinToKey = ArcotAIDMobile.applyPinToKey, ArcotAIDMobile.BitStringRep = function() {
    this.value = new Array
}, ArcotAIDMobile.getBytesToSign = function(e) {
    var t = new Array(48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0),
        n = ArcotSHA1.core_sha1(ArcotAIDMobile.bytear2intar(e), e.length * 8);
    n = ArcotAIDMobile.intar2bytear(n);
    var r = new Array(48, 33, 48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0, 4, 20);
    for (var i = 0; i < n.length; i++) r[r.length] = n[i];
    return r
}, ArcotAIDMobile.arcotsign = function(e, n, r) {
    hlen = 20, obits = e.bitLength() - 1, obytes = (obits + 7) / 8 & 4294967295, t = 8 * obytes - obits, tmask = 255 >> t;
    var i = new Array;
    for (var s = 0; s < 8; s++) i[s] = 0;
    var o = new Array;
    ArcotAIDMobile.fixedRandom(o, hlen);
    var u = "salt is: ";
    for (var s = 0; s < o.length; s++) u += o[s] + " ";
    var a = new Array;
    ArcotAIDMobile.arraycopy(i, 0, a, 0, 8);
    var f = r;
    ArcotAIDMobile.arraycopy(f, 0, a, 8, f.length), ArcotAIDMobile.arraycopy(o, 0, a, a.length, o.length);
    var l = ArcotSHA1.core_sha1(ArcotAIDMobile.bytear2intar(a), a.length * 8);
    l = ArcotAIDMobile.intar2bytear(l);
    var c = new Array;
    ArcotAIDMobile.arraycopy(o, 0, c, obytes - hlen - 1 - o.length, o.length), c[obytes - hlen - 1 - o.length - 1] = 1;
    for (var s = 0; s < obytes - hlen - 1 - o.length - 1; s++) c[s] = 0;
    var h = ArcotUtil.util_hex2tstring(ArcotAIDMobile.bytear2hexstr(l)),
        p = ArcotCrypto.crypto_mgf1(h, c.length),
        d = ArcotUtil.util_tstring2hex(p),
        v = ArcotAIDMobile.hexstr2bytes(d);
    for (var s = 0; s < v.length; ++s) c[s] ^= v[s];
    c[0] &= tmask;
    var m = new Array;
    ArcotAIDMobile.arraycopy(c, 0, m, 0, c.length), ArcotAIDMobile.arraycopy(l, 0, m, c.length, l.length), m[m.length] = 188;
    var g = ArcotAIDMobile.bytear2hexstr(m),
        y = new ArcotJSBN.BigInteger(g, 16),
        b = y.modPow(n, e);
    return b.toString(16)
}, ArcotAIDMobile.getLengthBytes = function(e) {
    var t;
    if (e < 128) t = new Array, t[0] = e;
    else {
        var n = 1,
            r = 256;
        while (r < e) r <<= 8, n++;
        var i = new Array;
        t = new Array, t[0] = 128 | n, n--;
        while (e > 0) i[n--] = e % 256, e >>= 8;
        for (var s = 0; s < i.length; s++) t[1 + s] = i[s]
    }
    return t
};
var ArcotASN1JS = {};
ArcotASN1JS.startdn = new Date, ArcotASN1JS.ID = new Array, ArcotASN1JS.NAME = new Array, ArcotASN1JS.ID.BOOLEAN = 1, ArcotASN1JS.ID.INTEGER = 2, ArcotASN1JS.ID.BITSTRING = 3, ArcotASN1JS.ID.OCTETSTRING = 4, ArcotASN1JS.ID.NULL = 5, ArcotASN1JS.ID.OBJECTIDENTIFIER = 6, ArcotASN1JS.ID.ObjectDescripter = 7, ArcotASN1JS.ID.UTF8String = 12, ArcotASN1JS.ID.SEQUENCE = 16, ArcotASN1JS.ID.SET = 17, ArcotASN1JS.ID.NumericString = 18, ArcotASN1JS.ID.PrintableString = 19, ArcotASN1JS.ID.TeletexString = 20, ArcotASN1JS.ID.IA5String = 22, ArcotASN1JS.ID.UTCTime = 23, ArcotASN1JS.ID.GeneralizedTime = 24, ArcotASN1JS.idx;
for (ArcotASN1JS.idx in ArcotASN1JS.ID) ArcotASN1JS.NAME[ArcotASN1JS.ID[ArcotASN1JS.idx]] = ArcotASN1JS.idx;
ArcotASN1JS.OID = new Array, ArcotASN1JS.TAB = "                              ", ArcotASN1JS.TAB_num = -1, ArcotASN1JS.Bitstring_hex_limit = 4, ArcotASN1JS.isEncode = new RegExp("[^0-9a-zA-Z/=+]", "i"), ArcotASN1JS.isB64 = new RegExp("[^0-9a-fA-F]", "i"), ArcotASN1JS.convert = function(e, t, n) {
    var r = e.value.replace(/[\s\r\n]/g, "");
    n == "auto" && (r.match(ArcotASN1JS.isEncode) ? n = "encode" : r.match(ArcotASN1JS.isB64) ? n = "decode_B64" : n = "decode_HEX");
    if (n == "encode") {
        t.value = ArcotASN1JS.encode(r);
        return
    }
    if (n == "decode_B64") {
        if (r.match(ArcotASN1JS.isEncode)) {
            if (confirm("Illegal character for Decoding process.\nDo you wish to continue as Encoding process?")) {
                t.value = ArcotASN1JS.encode(r);
                return
            }
            return
        }
        t.value = ArcotASN1JS.decode(ArcotASN1JS.bin2hex(ArcotASN1JS.base64decode(r)))
    } else if (n == "decode_HEX") {
        if (r.match(ArcotASN1JS.isB64)) {
            if (confirm("Illegal character for Decoding process.\nDo you wish to continue as Encoding process?")) {
                t.value = ArcotASN1JS.encode(r);
                return
            }
            return
        }
        t.value = ArcotASN1JS.decode(r)
    }
}, ArcotASN1JS.encode = function(e) {
    var t;
    return t
}, ArcotASN1JS.lastcontextvalue, ArcotASN1JS.decode = function(e, t) {
    return e.length % 2 != 0 && alert("Illegal length. Hex string's length must be even."), ArcotASN1JS.readASN1(e, t, 0)
}, ArcotASN1JS.readASN1 = function(e, t, n) {
    var r = 0,
        i = 0,
        s = 0,
        o = "";
    ArcotASN1JS.TAB_num++;
    while (r < e.length) {
        var u = parseInt("0x" + e.substr(r, 2)),
            a = u & 32,
            f = u & 128,
            l = u & 31,
            c = f ? "[" + l + "]" : ArcotASN1JS.NAME[l];
        c == undefined && (c = "Unsupported_TAG"), r += 2;
        var h = 0;
        if (l != 5) {
            if (parseInt("0x" + e.substr(r, 2)) & 128) {
                var p = parseInt("0x" + e.substr(r, 2)) & 127;
                if (p > 4) {
                    var d = "LENGTH field is too long.(at " + r + ")\nThis program accepts up to 4 octets of Length field.";
                    return d
                }
                h = parseInt("0x" + e.substr(r + 2, p * 2)), r += p * 2 + 2
            } else p != 0 && (h = parseInt("0x" + e.substr(r, 2)), r += 2);
            if (h > e.length - r) {
                var d = "LENGTH is longer than the rest.\n";
                return 0 / 0 + h + ", rest: " + e.length + ")", d
            }
        } else r += 2;
        var v = "",
            m = ArcotASN1JS.TAB.substr(0, ArcotASN1JS.TAB_num * 3);
        h && (v = e.substr(r, h * 2), a ? i = r : s = r, r += h * 2), f && (ArcotASN1JS.lastcontextvalue = v, t.addTag("CONTEXTSPECIFIC", l, r)), o += m + c + " ";
        if (a) t.addTag("SEQUENCE", null, r), o += "{\n" + ArcotASN1JS.readASN1(v, t, n + i) + m + "}", t.addTag("ENDSEQUENCE", null, r);
        else {
            var g = ArcotASN1JS.getValue(f ? 4 : l, v);
            t != null && t.addTag != undefined && t.addTag(c, g, r, n + s), o += g
        }
        o += "\n"
    }
    return ArcotASN1JS.TAB_num--, o
}, ArcotASN1JS.getValue = function(e, t) {
    var n = "";
    if (e == 1) n = t ? "TRUE" : "FALSE";
    else if (e == 2) n = parseInt("0x" + t);
    else if (e == 3) {
        var r = parseInt("0x" + t.substr(0, 2)),
            i = t.substr(2);
        i.length > ArcotASN1JS.Bitstring_hex_limit ? n = "0x" + i : n = parseInt("0x" + i).toString(2), n += " : " + r + " unused bit(s)"
    } else if (e == 5) n = "";
    else if (e == 6) {
        var s = new Array,
            o = parseInt("0x" + t.substr(0, 2));
        s[0] = Math.floor(o / 40), s[1] = o - s[0] * 40;
        var u = new Array,
            a = 0,
            f;
        for (f = 1; f < t.length - 2; f += 2) {
            var l = parseInt("0x" + t.substr(f + 1, 2));
            u.push(l & 127);
            if (l & 128) a++;
            else {
                var c, h = 0;
                for (c = 0; c < u.length; c++) h += u[c] * Math.pow(128, a--);
                s.push(h), a = 0, u = new Array
            }
        }
        n = s.join("."), ArcotASN1JS.OID[n] && (n += " (" + ArcotASN1JS.OID[n] + ")")
    } else if (e == 12) n = ArcotUtil.util_hex2str(t);
    else if (ArcotASN1JS.NAME[e].match(/(Time|String)$/)) {
        var p = 0;
        while (p < t.length) n += String.fromCharCode("0x" + t.substr(p, 2)), p += 2
    } else n = t;
    return n
}, ArcotASN1JS.init_oid = function(e) {
    var t = new Array;
    t = e.split(/\r?\n/);
    var n;
    for (n in t) {
        var r = new Array;
        r = t[n].split(/,/);
        var i;
        for (i in r) r[i] = r[i].replace(/^\s+/), r[i] = r[i].replace(/\s+$/);
        if (r.length < 2 || r[0].match(/^#/)) continue;
        r[0].match(/[^0-9\.\-\s]/) ? ArcotASN1JS.OID[r[1]] = r[0] : ArcotASN1JS.OID[r[0]] = r[1]
    }
}, ArcotASN1JS.bin2hex = function(e) {
    var t = "",
        n = 0,
        r = e.length;
    while (n < r) {
        var i = e.charCodeAt(n++).toString(16);
        i.length < 2 && (t += "0"), t += i
    }
    return t
}, ArcotASN1JS.base64chr = new Array(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1), ArcotASN1JS.base64decode = function(e) {
    var t, n, r, i, s, o, u;
    o = e.length, s = 0, u = "";
    while (s < o) {
        do t = ArcotASN1JS.base64chr[e.charCodeAt(s++) & 255]; while (s < o && t == -1);
        if (t == -1) break;
        do n = ArcotASN1JS.base64chr[e.charCodeAt(s++) & 255]; while (s < o && n == -1);
        if (n == -1) break;
        u += String.fromCharCode(t << 2 | (n & 48) >> 4);
        do {
            r = e.charCodeAt(s++) & 255;
            if (r == 61) return u;
            r = ArcotASN1JS.base64chr[r]
        } while (s < o && r == -1);
        if (r == -1) break;
        u += String.fromCharCode((n & 15) << 4 | (r & 60) >> 2);
        do {
            i = e.charCodeAt(s++) & 255;
            if (i == 61) return u;
            i = ArcotASN1JS.base64chr[i]
        } while (s < o && i == -1);
        if (i == -1) break;
        u += String.fromCharCode((r & 3) << 6 | i)
    }
    return u
};
var ArcotBase64 = {
        b64map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        b64padding: "=",
        hex2b64: function(e) {
            var t, n, r = "";
            for (t = 0; t + 3 <= e.length; t += 3) n = parseInt(e.substring(t, t + 3), 16), r += ArcotBase64.b64map.charAt(n >> 6) + ArcotBase64.b64map.charAt(n & 63);
            t + 1 == e.length ? (n = parseInt(e.substring(t, t + 1), 16), r += ArcotBase64.b64map.charAt(n << 2)) : t + 2 == e.length && (n = parseInt(e.substring(t, t + 2), 16), r += ArcotBase64.b64map.charAt(n >> 2) + ArcotBase64.b64map.charAt((n & 3) << 4));
            while ((r.length & 3) > 0) r += ArcotBase64.b64padding;
            return r
        },
        b64tohex: function(e) {
            var t = "",
                n, r = 0,
                i;
            for (n = 0; n < e.length; ++n) {
                if (e.charAt(n) == ArcotBase64.b64padding) break;
                v = ArcotBase64.b64map.indexOf(e.charAt(n));
                if (v < 0) continue;
                r == 0 ? (t += ArcotJSBN.int2char(v >> 2), i = v & 3, r = 1) : r == 1 ? (t += ArcotJSBN.int2char(i << 2 | v >> 4), i = v & 15, r = 2) : r == 2 ? (t += ArcotJSBN.int2char(i), t += ArcotJSBN.int2char(v >> 2), i = v & 3, r = 3) : (t += ArcotJSBN.int2char(i << 2 | v >> 4), t += ArcotJSBN.int2char(v & 15), r = 0)
            }
            return r == 1 && (t += ArcotJSBN.int2char(i << 2)), t
        },
        b64toBA: function(e) {
            var t = ArcotBase64.b64tohex(e),
                n, r = new Array;
            for (n = 0; 2 * n < t.length; ++n) r[n] = parseInt(t.substring(2 * n, 2 * n + 2), 16);
            return r
        }
    },
    ArcotCrypto = {
        nullBlock: "0000000000000000",
        crypto_des: function(e, t, n, r, i, s) {
            return des(e, t, n, r, i, s)
        },
        crypto_str_sha1: function(e) {
            return ArcotSHA1.str_sha1(e)
        },
        crypto_X919Digest: function(e, t, n) {
            n.length % 8 > 0 ? n += ArcotCrypto.nullBlock.substr(0, 8 - n.length % 8) : n.length == 0 && (n = ArcotCrypto.nullBlock);
            var r = ArcotUtil.util_hex2tstring(e),
                i = ArcotUtil.util_hex2tstring(t),
                s = ArcotUtil.util_hex2tstring(n),
                o = ArcotUtil.util_hex2tstring(ArcotCrypto.nullBlock),
                u = ArcotCrypto.crypto_des(r, s, 1, 1, o, 3),
                a = ArcotCrypto.crypto_des(i, u.substr(u.length - 8, 8), 0, 0),
                f = ArcotCrypto.crypto_des(r, a, 1, 0),
                l = ArcotUtil.util_tstring2hex(f);
            return l
        },
        crypto_mgf1: function(e, t) {
            var n = new Array(4),
                r, i = 0,
                s = "";
            for (var o = 0; i < t; o++) n[0] = o >> 24 & 255, n[1] = o >> 16 & 255, n[2] = o >> 8 & 255, n[3] = o & 255, cntstr = String.fromCharCode(n[0]) + String.fromCharCode(n[1]) + String.fromCharCode(n[2]) + String.fromCharCode(n[3]), r = ArcotCrypto.crypto_str_sha1(e + cntstr), i + r.length > t ? (s += r.substr(0, t - i), i = t) : (s += r, i += r.length);
            return s
        }
    },
    ArcotJSBN = {};
ArcotJSBN.dbits, ArcotJSBN.canary = 0xdeadbeefcafe, ArcotJSBN.j_lm = (ArcotJSBN.canary & 16777215) == 15715070, ArcotJSBN.BigInteger = function(e, t, n) {
    e != null && ("number" == typeof e ? this.fromNumber(e, t, n) : t == null && "string" != typeof e ? this.fromString(e, 256) : this.fromString(e, t))
}, ArcotJSBN.nbi = function() {
    return new ArcotJSBN.BigInteger(null)
}, ArcotJSBN.am1 = function(e, t, n, r, i, s) {
    while (--s >= 0) {
        var o = t * this[e++] + n[r] + i;
        i = Math.floor(o / 67108864), n[r++] = o & 67108863
    }
    return i
}, ArcotJSBN.am2 = function(e, t, n, r, i, s) {
    var o = t & 32767,
        u = t >> 15;
    while (--s >= 0) {
        var a = this[e] & 32767,
            f = this[e++] >> 15,
            l = u * a + f * o;
        a = o * a + ((l & 32767) << 15) + n[r] + (i & 1073741823), i = (a >>> 30) + (l >>> 15) + u * f + (i >>> 30), n[r++] = a & 1073741823
    }
    return i
}, ArcotJSBN.am3 = function(e, t, n, r, i, s) {
    var o = t & 16383,
        u = t >> 14;
    while (--s >= 0) {
        var a = this[e] & 16383,
            f = this[e++] >> 14,
            l = u * a + f * o;
        a = o * a + ((l & 16383) << 14) + n[r] + i, i = (a >> 28) + (l >> 14) + u * f, n[r++] = a & 268435455
    }
    return i
}, ArcotJSBN.j_lm && navigator.appName == "Microsoft Internet Explorer" ? (ArcotJSBN.BigInteger.prototype.am = ArcotJSBN.am2, ArcotJSBN.dbits = 30) : ArcotJSBN.j_lm && navigator.appName != "Netscape" ? (ArcotJSBN.BigInteger.prototype.am = ArcotJSBN.am1, ArcotJSBN.dbits = 26) : ArcotJSBN.j_lm && navigator.appName != "BlackBerry" ? (ArcotJSBN.BigInteger.prototype.am = ArcotJSBN.am2, ArcotJSBN.dbits = 30) : (ArcotJSBN.BigInteger.prototype.am = ArcotJSBN.am3, ArcotJSBN.dbits = 28), ArcotJSBN.BigInteger.prototype.DB = ArcotJSBN.dbits, ArcotJSBN.BigInteger.prototype.DM = (1 << ArcotJSBN.dbits) - 1, ArcotJSBN.BigInteger.prototype.DV = 1 << ArcotJSBN.dbits, ArcotJSBN.BI_FP = 52, ArcotJSBN.BigInteger.prototype.FV = Math.pow(2, ArcotJSBN.BI_FP), ArcotJSBN.BigInteger.prototype.F1 = ArcotJSBN.BI_FP - ArcotJSBN.dbits, ArcotJSBN.BigInteger.prototype.F2 = 2 * ArcotJSBN.dbits - ArcotJSBN.BI_FP, ArcotJSBN.BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz", ArcotJSBN.BI_RC = new Array, ArcotJSBN.rr, ArcotJSBN.vv, ArcotJSBN.rr = "0".charCodeAt(0);
for (ArcotJSBN.vv = 0; ArcotJSBN.vv <= 9; ++ArcotJSBN.vv) ArcotJSBN.BI_RC[ArcotJSBN.rr++] = ArcotJSBN.vv;
ArcotJSBN.rr = "a".charCodeAt(0);
for (ArcotJSBN.vv = 10; ArcotJSBN.vv < 36; ++ArcotJSBN.vv) ArcotJSBN.BI_RC[ArcotJSBN.rr++] = ArcotJSBN.vv;
ArcotJSBN.rr = "A".charCodeAt(0);
for (ArcotJSBN.vv = 10; ArcotJSBN.vv < 36; ++ArcotJSBN.vv) ArcotJSBN.BI_RC[ArcotJSBN.rr++] = ArcotJSBN.vv;
ArcotJSBN.int2char = function(e) {
    return ArcotJSBN.BI_RM.charAt(e)
}, ArcotJSBN.intAt = function(e, t) {
    return ArcotJSBN.c = ArcotJSBN.BI_RC[e.charCodeAt(t)], ArcotJSBN.c == null ? -1 : ArcotJSBN.c
}, ArcotJSBN.bnpCopyTo = function(e) {
    for (var t = this.t - 1; t >= 0; --t) e[t] = this[t];
    e.t = this.t, e.s = this.s
}, ArcotJSBN.bnpFromInt = function(e) {
    this.t = 1, this.s = e < 0 ? -1 : 0, e > 0 ? this[0] = e : e < -1 ? this[0] = e + DV : this.t = 0
}, ArcotJSBN.nbv = function(e) {
    var t = ArcotJSBN.nbi();
    return t.fromInt(e), t
}, ArcotJSBN.bnpFromString = function(e, t) {
    var n;
    if (t == 16) n = 4;
    else if (t == 8) n = 3;
    else if (t == 256) n = 8;
    else if (t == 2) n = 1;
    else if (t == 32) n = 5;
    else {
        if (t != 4) {
            this.fromRadix(e, t);
            return
        }
        n = 2
    }
    this.t = 0, this.s = 0;
    var r = e.length,
        i = !1,
        s = 0;
    while (--r >= 0) {
        var o = n == 8 ? e[r] & 255 : ArcotJSBN.intAt(e, r);
        if (o < 0) {
            e.charAt(r) == "-" && (i = !0);
            continue
        }
        i = !1, s == 0 ? this[this.t++] = o : s + n > this.DB ? (this[this.t - 1] |= (o & (1 << this.DB - s) - 1) << s, this[this.t++] = o >> this.DB - s) : this[this.t - 1] |= o << s, s += n, s >= this.DB && (s -= this.DB)
    }
    n == 8 && (e[0] & 128) != 0 && (this.s = -1, s > 0 && (this[this.t - 1] |= (1 << this.DB - s) - 1 << s)), this.clamp(), i && ArcotJSBN.BigInteger.ZERO.subTo(this, this)
}, ArcotJSBN.bnpClamp = function() {
    var e = this.s & this.DM;
    while (this.t > 0 && this[this.t - 1] == e) --this.t
}, ArcotJSBN.bnToString = function(e) {
    if (this.s < 0) return "-" + this.negate().toString(e);
    var t;
    if (e == 16) t = 4;
    else if (e == 8) t = 3;
    else if (e == 2) t = 1;
    else if (e == 32) t = 5;
    else {
        if (e != 4) return this.toRadix(e);
        t = 2
    }
    var n = (1 << t) - 1,
        r, i = !1,
        s = "",
        o = this.t,
        u = this.DB - o * this.DB % t;
    if (o-- > 0) {
        u < this.DB && (r = this[o] >> u) > 0 && (i = !0, s = ArcotJSBN.int2char(r));
        while (o >= 0) u < t ? (r = (this[o] & (1 << u) - 1) << t - u, r |= this[--o] >> (u += this.DB - t)) : (r = this[o] >> (u -= t) & n, u <= 0 && (u += this.DB, --o)), r > 0 && (i = !0), i && (s += ArcotJSBN.int2char(r))
    }
    return i ? s : "0"
}, ArcotJSBN.bnNegate = function() {
    var e = ArcotJSBN.nbi();
    return ArcotJSBN.BigInteger.ZERO.subTo(this, e), e
}, ArcotJSBN.bnAbs = function() {
    return this.s < 0 ? this.negate() : this
}, ArcotJSBN.bnCompareTo = function(e) {
    var t = this.s - e.s;
    if (t != 0) return t;
    var n = this.t;
    t = n - e.t;
    if (t != 0) return t;
    while (--n >= 0)
        if ((t = this[n] - e[n]) != 0) return t;
    return 0
}, ArcotJSBN.nbits = function(e) {
    var t = 1,
        n;
    return (n = e >>> 16) != 0 && (e = n, t += 16), (n = e >> 8) != 0 && (e = n, t += 8), (n = e >> 4) != 0 && (e = n, t += 4), (n = e >> 2) != 0 && (e = n, t += 2), (n = e >> 1) != 0 && (e = n, t += 1), t
}, ArcotJSBN.bnBitLength = function() {
    return this.t <= 0 ? 0 : this.DB * (this.t - 1) + ArcotJSBN.nbits(this[this.t - 1] ^ this.s & this.DM)
}, ArcotJSBN.bnpDLShiftTo = function(e, t) {
    var n;
    for (n = this.t - 1; n >= 0; --n) t[n + e] = this[n];
    for (n = e - 1; n >= 0; --n) t[n] = 0;
    t.t = this.t + e, t.s = this.s
}, ArcotJSBN.bnpDRShiftTo = function(e, t) {
    for (var n = e; n < this.t; ++n) t[n - e] = this[n];
    t.t = Math.max(this.t - e, 0), t.s = this.s
}, ArcotJSBN.bnpLShiftTo = function(e, t) {
    var n = e % this.DB,
        r = this.DB - n,
        i = (1 << r) - 1,
        s = Math.floor(e / this.DB),
        o = this.s << n & this.DM,
        u;
    for (u = this.t - 1; u >= 0; --u) t[u + s + 1] = this[u] >> r | o, o = (this[u] & i) << n;
    for (u = s - 1; u >= 0; --u) t[u] = 0;
    t[s] = o, t.t = this.t + s + 1, t.s = this.s, t.clamp()
}, ArcotJSBN.bnpRShiftTo = function(e, t) {
    t.s = this.s;
    var n = Math.floor(e / this.DB);
    if (n >= this.t) {
        t.t = 0;
        return
    }
    var r = e % this.DB,
        i = this.DB - r,
        s = (1 << r) - 1;
    t[0] = this[n] >> r;
    for (var o = n + 1; o < this.t; ++o) t[o - n - 1] |= (this[o] & s) << i, t[o - n] = this[o] >> r;
    r > 0 && (t[this.t - n - 1] |= (this.s & s) << i), t.t = this.t - n, t.clamp()
}, ArcotJSBN.bnpSubTo = function(e, t) {
    var n = 0,
        r = 0,
        i = Math.min(e.t, this.t);
    while (n < i) r += this[n] - e[n], t[n++] = r & this.DM, r >>= this.DB;
    if (e.t < this.t) {
        r -= e.s;
        while (n < this.t) r += this[n], t[n++] = r & this.DM, r >>= this.DB;
        r += this.s
    } else {
        r += this.s;
        while (n < e.t) r -= e[n], t[n++] = r & this.DM, r >>= this.DB;
        r -= e.s
    }
    t.s = r < 0 ? -1 : 0, r < -1 ? t[n++] = this.DV + r : r > 0 && (t[n++] = r), t.t = n, t.clamp()
}, ArcotJSBN.bnpMultiplyTo = function(e, t) {
    var n = this.abs(),
        r = e.abs(),
        i = n.t;
    t.t = i + r.t;
    while (--i >= 0) t[i] = 0;
    for (i = 0; i < r.t; ++i) t[i + n.t] = n.am(0, r[i], t, i, 0, n.t);
    t.s = 0, t.clamp(), this.s != e.s && ArcotJSBN.BigInteger.ZERO.subTo(t, t)
}, ArcotJSBN.bnpSquareTo = function(e) {
    var t = this.abs(),
        n = e.t = 2 * t.t;
    while (--n >= 0) e[n] = 0;
    for (n = 0; n < t.t - 1; ++n) {
        var r = t.am(n, t[n], e, 2 * n, 0, 1);
        (e[n + t.t] += t.am(n + 1, 2 * t[n], e, 2 * n + 1, r, t.t - n - 1)) >= t.DV && (e[n + t.t] -= t.DV, e[n + t.t + 1] = 1)
    }
    e.t > 0 && (e[e.t - 1] += t.am(n, t[n], e, 2 * n, 0, 1)), e.s = 0, e.clamp()
}, ArcotJSBN.bnpDivRemTo = function(e, t, n) {
    var r = e.abs();
    if (r.t <= 0) return;
    var i = this.abs();
    if (i.t < r.t) {
        t != null && t.fromInt(0), n != null && this.copyTo(n);
        return
    }
    n == null && (n = ArcotJSBN.nbi());
    var s = ArcotJSBN.nbi(),
        o = this.s,
        u = e.s,
        a = this.DB - ArcotJSBN.nbits(r[r.t - 1]);
    a > 0 ? (r.lShiftTo(a, s), i.lShiftTo(a, n)) : (r.copyTo(s), i.copyTo(n));
    var f = s.t,
        l = s[f - 1];
    if (l == 0) return;
    var c = l * (1 << this.F1) + (f > 1 ? s[f - 2] >> this.F2 : 0),
        h = this.FV / c,
        p = (1 << this.F1) / c,
        d = 1 << this.F2,
        v = n.t,
        m = v - f,
        g = t == null ? ArcotJSBN.nbi() : t;
    s.dlShiftTo(m, g), n.compareTo(g) >= 0 && (n[n.t++] = 1, n.subTo(g, n)), ArcotJSBN.BigInteger.ONE.dlShiftTo(f, g), g.subTo(s, s);
    while (s.t < f) s[s.t++] = 0;
    while (--m >= 0) {
        var y = n[--v] == l ? this.DM : Math.floor(n[v] * h + (n[v - 1] + d) * p);
        if ((n[v] += s.am(0, y, n, m, 0, f)) < y) {
            s.dlShiftTo(m, g), n.subTo(g, n);
            while (n[v] < --y) n.subTo(g, n)
        }
    }
    t != null && (n.drShiftTo(f, t), o != u && ArcotJSBN.BigInteger.ZERO.subTo(t, t)), n.t = f, n.clamp(), a > 0 && n.rShiftTo(a, n), o < 0 && ArcotJSBN.BigInteger.ZERO.subTo(n, n)
}, ArcotJSBN.bnMod = function(e) {
    var t = ArcotJSBN.nbi();
    return this.abs().divRemTo(e, null, t), this.s < 0 && t.compareTo(ArcotJSBN.BigInteger.ZERO) > 0 && e.subTo(t, t), t
}, ArcotJSBN.Classic = function(e) {
    this.m = e
}, ArcotJSBN.cConvert = function(e) {
    return e.s < 0 || e.compareTo(this.m) >= 0 ? e.mod(this.m) : e
}, ArcotJSBN.cRevert = function(e) {
    return e
}, ArcotJSBN.cReduce = function(e) {
    e.divRemTo(this.m, null, e)
}, ArcotJSBN.cMulTo = function(e, t, n) {
    e.multiplyTo(t, n), this.reduce(n)
}, ArcotJSBN.cSqrTo = function(e, t) {
    e.squareTo(t), this.reduce(t)
}, ArcotJSBN.Classic.prototype.convert = ArcotJSBN.cConvert, ArcotJSBN.Classic.prototype.revert = ArcotJSBN.cRevert, ArcotJSBN.Classic.prototype.reduce = ArcotJSBN.cReduce, ArcotJSBN.Classic.prototype.mulTo = ArcotJSBN.cMulTo, ArcotJSBN.Classic.prototype.sqrTo = ArcotJSBN.cSqrTo, ArcotJSBN.bnpInvDigit = function() {
    if (this.t < 1) return 0;
    var e = this[0];
    if ((e & 1) == 0) return 0;
    var t = e & 3;
    return t = t * (2 - (e & 15) * t) & 15, t = t * (2 - (e & 255) * t) & 255, t = t * (2 - ((e & 65535) * t & 65535)) & 65535, t = t * (2 - e * t % this.DV) % this.DV, t > 0 ? this.DV - t : -t
}, ArcotJSBN.Montgomery = function(e) {
    this.m = e, this.mp = e.invDigit(), this.mpl = this.mp & 32767, this.mph = this.mp >> 15, this.um = (1 << e.DB - 15) - 1, this.mt2 = 2 * e.t
}, ArcotJSBN.montConvert = function(e) {
    var t = ArcotJSBN.nbi();
    return e.abs().dlShiftTo(this.m.t, t), t.divRemTo(this.m, null, t), e.s < 0 && t.compareTo(ArcotJSBN.BigInteger.ZERO) > 0 && this.m.subTo(t, t), t
}, ArcotJSBN.montRevert = function(e) {
    var t = ArcotJSBN.nbi();
    return e.copyTo(t), this.reduce(t), t
}, ArcotJSBN.montReduce = function(e) {
    while (e.t <= this.mt2) e[e.t++] = 0;
    for (var t = 0; t < this.m.t; ++t) {
        var n = e[t] & 32767,
            r = n * this.mpl + ((n * this.mph + (e[t] >> 15) * this.mpl & this.um) << 15) & e.DM;
        n = t + this.m.t, e[n] += this.m.am(0, r, e, t, 0, this.m.t);
        while (e[n] >= e.DV) e[n] -= e.DV, e[++n]++
    }
    e.clamp(), e.drShiftTo(this.m.t, e), e.compareTo(this.m) >= 0 && e.subTo(this.m, e)
}, ArcotJSBN.montSqrTo = function(e, t) {
    e.squareTo(t), this.reduce(t)
}, ArcotJSBN.montMulTo = function(e, t, n) {
    e.multiplyTo(t, n), this.reduce(n)
}, ArcotJSBN.Montgomery.prototype.convert = ArcotJSBN.montConvert, ArcotJSBN.Montgomery.prototype.revert = ArcotJSBN.montRevert, ArcotJSBN.Montgomery.prototype.reduce = ArcotJSBN.montReduce, ArcotJSBN.Montgomery.prototype.mulTo = ArcotJSBN.montMulTo, ArcotJSBN.Montgomery.prototype.sqrTo = ArcotJSBN.montSqrTo, ArcotJSBN.bnpIsEven = function() {
    return (this.t > 0 ? this[0] & 1 : this.s) == 0
}, ArcotJSBN.bnpExp = function(e, t) {
    if (e > 4294967295 || e < 1) return ArcotJSBN.BigInteger.ONE;
    var n = ArcotJSBN.nbi(),
        r = ArcotJSBN.nbi(),
        i = t.convert(this),
        s = ArcotJSBN.nbits(e) - 1;
    i.copyTo(n);
    while (--s >= 0) {
        t.sqrTo(n, r);
        if ((e & 1 << s) > 0) t.mulTo(r, i, n);
        else {
            var o = n;
            n = r, r = o
        }
    }
    return t.revert(n)
}, ArcotJSBN.bnModPowInt = function(e, t) {
    var n;
    return e < 256 || t.isEven() ? n = new ArcotJSBN.Classic(t) : n = new ArcotJSBN.Montgomery(t), this.exp(e, n)
}, ArcotJSBN.BigInteger.prototype.copyTo = ArcotJSBN.bnpCopyTo, ArcotJSBN.BigInteger.prototype.fromInt = ArcotJSBN.bnpFromInt, ArcotJSBN.BigInteger.prototype.fromString = ArcotJSBN.bnpFromString, ArcotJSBN.BigInteger.prototype.clamp = ArcotJSBN.bnpClamp, ArcotJSBN.BigInteger.prototype.dlShiftTo = ArcotJSBN.bnpDLShiftTo, ArcotJSBN.BigInteger.prototype.drShiftTo = ArcotJSBN.bnpDRShiftTo, ArcotJSBN.BigInteger.prototype.lShiftTo = ArcotJSBN.bnpLShiftTo, ArcotJSBN.BigInteger.prototype.rShiftTo = ArcotJSBN.bnpRShiftTo, ArcotJSBN.BigInteger.prototype.subTo = ArcotJSBN.bnpSubTo, ArcotJSBN.BigInteger.prototype.multiplyTo = ArcotJSBN.bnpMultiplyTo, ArcotJSBN.BigInteger.prototype.squareTo = ArcotJSBN.bnpSquareTo, ArcotJSBN.BigInteger.prototype.divRemTo = ArcotJSBN.bnpDivRemTo, ArcotJSBN.BigInteger.prototype.invDigit = ArcotJSBN.bnpInvDigit, ArcotJSBN.BigInteger.prototype.isEven = ArcotJSBN.bnpIsEven, ArcotJSBN.BigInteger.prototype.exp = ArcotJSBN.bnpExp, ArcotJSBN.BigInteger.prototype.toString = ArcotJSBN.bnToString, ArcotJSBN.BigInteger.prototype.negate = ArcotJSBN.bnNegate, ArcotJSBN.BigInteger.prototype.abs = ArcotJSBN.bnAbs, ArcotJSBN.BigInteger.prototype.compareTo = ArcotJSBN.bnCompareTo, ArcotJSBN.BigInteger.prototype.bitLength = ArcotJSBN.bnBitLength, ArcotJSBN.BigInteger.prototype.mod = ArcotJSBN.bnMod, ArcotJSBN.BigInteger.prototype.modPowInt = ArcotJSBN.bnModPowInt, ArcotJSBN.BigInteger.ZERO = ArcotJSBN.nbv(0), ArcotJSBN.BigInteger.ONE = ArcotJSBN.nbv(1), ArcotJSBN.bnClone = function() {
    var e = ArcotJSBN.nbi();
    return this.copyTo(e), e
}, ArcotJSBN.bnIntValue = function() {
    if (this.s < 0) {
        if (this.t == 1) return this[0] - this.DV;
        if (this.t == 0) return -1
    } else {
        if (this.t == 1) return this[0];
        if (this.t == 0) return 0
    }
    return (this[1] & (1 << 32 - this.DB) - 1) << this.DB | this[0]
}, ArcotJSBN.bnByteValue = function() {
    return this.t == 0 ? this.s : this[0] << 24 >> 24
}, ArcotJSBN.bnShortValue = function() {
    return this.t == 0 ? this.s : this[0] << 16 >> 16
}, ArcotJSBN.bnpChunkSize = function(e) {
    return Math.floor(Math.LN2 * this.DB / Math.log(e))
}, ArcotJSBN.bnSigNum = function() {
    return this.s < 0 ? -1 : this.t <= 0 || this.t == 1 && this[0] <= 0 ? 0 : 1
}, ArcotJSBN.bnpToRadix = function(e) {
    e == null && (e = 10);
    if (this.signum() == 0 || e < 2 || e > 36) return "0";
    var t = this.chunkSize(e),
        n = Math.pow(e, t),
        r = ArcotJSBN.nbv(n),
        i = ArcotJSBN.nbi(),
        s = ArcotJSBN.nbi(),
        o = "";
    this.divRemTo(r, i, s);
    while (i.signum() > 0) o = (n + s.intValue()).toString(e).substr(1) + o, i.divRemTo(r, i, s);
    return s.intValue().toString(e) + o
}, ArcotJSBN.bnpFromRadix = function(e, t) {
    this.fromInt(0), t == null && (t = 10);
    var n = this.chunkSize(t),
        r = Math.pow(t, n),
        i = !1,
        s = 0,
        o = 0;
    for (var u = 0; u < e.length; ++u) {
        var a = ArcotJSBN.intAt(e, u);
        if (a < 0) {
            e.charAt(u) == "-" && this.signum() == 0 && (i = !0);
            continue
        }
        o = t * o + a, ++s >= n && (this.dMultiply(r), this.dAddOffset(o, 0), s = 0, o = 0)
    }
    s > 0 && (this.dMultiply(Math.pow(t, s)), this.dAddOffset(o, 0)), i && ArcotJSBN.BigInteger.ZERO.subTo(this, this)
}, ArcotJSBN.bnpFromNumber = function(e, t, n) {
    if ("number" == typeof t)
        if (e < 2) this.fromInt(1);
        else {
            this.fromNumber(e, n), this.testBit(e - 1) || this.bitwiseTo(ArcotJSBN.BigInteger.ONE.shiftLeft(e - 1), ArcotJSBN.op_or, this), this.isEven() && this.dAddOffset(1, 0);
            while (!this.isProbablePrime(t)) this.dAddOffset(2, 0), this.bitLength() > e && this.subTo(ArcotJSBN.BigInteger.ONE.shiftLeft(e - 1), this)
        }
    else {
        var r = new Array,
            i = e & 7;
        r.length = (e >> 3) + 1, t.nextBytes(r), i > 0 ? r[0] &= (1 << i) - 1 : r[0] = 0, this.fromString(r, 256)
    }
}, ArcotJSBN.bnToByteArray = function() {
    var e = this.t,
        t = new Array;
    t[0] = this.s;
    var n = this.DB - e * this.DB % 8,
        r, i = 0;
    if (e-- > 0) {
        n < this.DB && (r = this[e] >> n) != (this.s & this.DM) >> n && (t[i++] = r | this.s << this.DB - n);
        while (e >= 0) {
            n < 8 ? (r = (this[e] & (1 << n) - 1) << 8 - n, r |= this[--e] >> (n += this.DB - 8)) : (r = this[e] >> (n -= 8) & 255, n <= 0 && (n += this.DB, --e)), (r & 128) != 0 && (r |= -256), i == 0 && (this.s & 128) != (r & 128) && ++i;
            if (i > 0 || r != this.s) t[i++] = r
        }
    }
    return t
}, ArcotJSBN.bnEquals = function(e) {
    return this.compareTo(e) == 0
}, ArcotJSBN.bnMin = function(e) {
    return this.compareTo(e) < 0 ? this : e
}, ArcotJSBN.bnMax = function(e) {
    return this.compareTo(e) > 0 ? this : e
}, ArcotJSBN.bnpBitwiseTo = function(e, t, n) {
    var r, i, s = Math.min(e.t, this.t);
    for (r = 0; r < s; ++r) n[r] = t(this[r], e[r]);
    if (e.t < this.t) {
        i = e.s & this.DM;
        for (r = s; r < this.t; ++r) n[r] = t(this[r], i);
        n.t = this.t
    } else {
        i = this.s & this.DM;
        for (r = s; r < e.t; ++r) n[r] = t(i, e[r]);
        n.t = e.t
    }
    n.s = t(this.s, e.s), n.clamp()
}, ArcotJSBN.op_and = function(e, t) {
    return e & t
}, ArcotJSBN.bnAnd = function(e) {
    var t = ArcotJSBN.nbi();
    return this.bitwiseTo(e, ArcotJSBN.op_and, t), t
}, ArcotJSBN.op_or = function(e, t) {
    return e | t
}, ArcotJSBN.bnOr = function(e) {
    var t = ArcotJSBN.nbi();
    return this.bitwiseTo(e, ArcotJSBN.op_or, t), t
}, ArcotJSBN.op_xor = function(e, t) {
    return e ^ t
}, ArcotJSBN.bnXor = function(e) {
    var t = ArcotJSBN.nbi();
    return this.bitwiseTo(e, ArcotJSBN.op_xor, t), t
}, ArcotJSBN.op_andnot = function(e, t) {
    return e & ~t
}, ArcotJSBN.bnAndNot = function(e) {
    var t = ArcotJSBN.nbi();
    return this.bitwiseTo(e, ArcotJSBN.op_andnot, t), t
}, ArcotJSBN.bnNot = function() {
    var e = ArcotJSBN.nbi();
    for (var t = 0; t < this.t; ++t) e[t] = this.DM & ~this[t];
    return e.t = this.t, e.s = ~this.s, e
}, ArcotJSBN.bnShiftLeft = function(e) {
    var t = ArcotJSBN.nbi();
    return e < 0 ? this.rShiftTo(-e, t) : this.lShiftTo(e, t), t
}, ArcotJSBN.bnShiftRight = function(e) {
    var t = ArcotJSBN.nbi();
    return e < 0 ? this.lShiftTo(-e, t) : this.rShiftTo(e, t), t
}, ArcotJSBN.lbit = function(e) {
    if (e == 0) return -1;
    var t = 0;
    return (e & 65535) == 0 && (e >>= 16, t += 16), (e & 255) == 0 && (e >>= 8, t += 8), (e & 15) == 0 && (e >>= 4, t += 4), (e & 3) == 0 && (e >>= 2, t += 2), (e & 1) == 0 && ++t, t
}, ArcotJSBN.bnGetLowestSetBit = function() {
    for (var e = 0; e < this.t; ++e)
        if (this[e] != 0) return e * this.DB + ArcotJSBN.lbit(this[e]);
    return this.s < 0 ? this.t * this.DB : -1
}, ArcotJSBN.cbit = function(e) {
    var t = 0;
    while (e != 0) e &= e - 1, ++t;
    return t
}, ArcotJSBN.bnBitCount = function() {
    var e = 0,
        t = this.s & this.DM;
    for (var n = 0; n < this.t; ++n) e += ArcotJSBN.cbit(this[n] ^ t);
    return e
}, ArcotJSBN.bnTestBit = function(e) {
    var t = Math.floor(e / this.DB);
    return t >= this.t ? this.s != 0 : (this[t] & 1 << e % this.DB) != 0
}, ArcotJSBN.bnpChangeBit = function(e, t) {
    var n = ArcotJSBN.BigInteger.ONE.shiftLeft(e);
    return this.bitwiseTo(n, t, n), n
}, ArcotJSBN.bnSetBit = function(e) {
    return this.changeBit(e, ArcotJSBN.op_or)
}, ArcotJSBN.bnClearBit = function(e) {
    return this.changeBit(e, ArcotJSBN.op_andnot)
}, ArcotJSBN.bnFlipBit = function(e) {
    return this.changeBit(e, ArcotJSBN.op_xor)
}, ArcotJSBN.bnpAddTo = function(e, t) {
    var n = 0,
        r = 0,
        i = Math.min(e.t, this.t);
    while (n < i) r += this[n] + e[n], t[n++] = r & this.DM, r >>= this.DB;
    if (e.t < this.t) {
        r += e.s;
        while (n < this.t) r += this[n], t[n++] = r & this.DM, r >>= this.DB;
        r += this.s
    } else {
        r += this.s;
        while (n < e.t) r += e[n], t[n++] = r & this.DM, r >>= this.DB;
        r += e.s
    }
    t.s = r < 0 ? -1 : 0, r > 0 ? t[n++] = r : r < -1 && (t[n++] = this.DV + r), t.t = n, t.clamp()
}, ArcotJSBN.bnAdd = function(e) {
    var t = ArcotJSBN.nbi();
    return this.addTo(e, t), t
}, ArcotJSBN.bnSubtract = function(e) {
    var t = ArcotJSBN.nbi();
    return this.subTo(e, t), t
}, ArcotJSBN.bnMultiply = function(e) {
    var t = ArcotJSBN.nbi();
    return this.multiplyTo(e, t), t
}, ArcotJSBN.bnDivide = function(e) {
    var t = ArcotJSBN.nbi();
    return this.divRemTo(e, t, null), t
}, ArcotJSBN.bnRemainder = function(e) {
    var t = ArcotJSBN.nbi();
    return this.divRemTo(e, null, t), t
}, ArcotJSBN.bnDivideAndRemainder = function(e) {
    var t = ArcotJSBN.nbi(),
        n = ArcotJSBN.nbi();
    return this.divRemTo(e, t, n), new Array(t, n)
}, ArcotJSBN.bnpDMultiply = function(e) {
    this[this.t] = this.am(0, e - 1, this, 0, 0, this.t), ++this.t, this.clamp()
}, ArcotJSBN.bnpDAddOffset = function(e, t) {
    while (this.t <= t) this[this.t++] = 0;
    this[t] += e;
    while (this[t] >= this.DV) this[t] -= this.DV, ++t >= this.t && (this[this.t++] = 0), ++this[t]
}, ArcotJSBN.NullExp = function() {}, ArcotJSBN.nNop = function(e) {
    return e
}, ArcotJSBN.nMulTo = function(e, t, n) {
    e.multiplyTo(t, n)
}, ArcotJSBN.nSqrTo = function(e, t) {
    e.squareTo(t)
}, ArcotJSBN.NullExp.prototype.convert = ArcotJSBN.nNop, ArcotJSBN.NullExp.prototype.revert = ArcotJSBN.nNop, ArcotJSBN.NullExp.prototype.mulTo = ArcotJSBN.nMulTo, ArcotJSBN.NullExp.prototype.sqrTo = ArcotJSBN.nSqrTo, ArcotJSBN.bnPow = function(e) {
    return this.exp(e, new ArcotJSBN.NullExp)
}, ArcotJSBN.bnpMultiplyLowerTo = function(e, t, n) {
    var r = Math.min(this.t + e.t, t);
    n.s = 0, n.t = r;
    while (r > 0) n[--r] = 0;
    var i;
    for (i = n.t - this.t; r < i; ++r) n[r + this.t] = this.am(0, e[r], n, r, 0, this.t);
    for (i = Math.min(e.t, t); r < i; ++r) this.am(0, e[r], n, r, 0, t - r);
    n.clamp()
}, ArcotJSBN.bnpMultiplyUpperTo = function(e, t, n) {
    --t;
    var r = n.t = this.t + e.t - t;
    n.s = 0;
    while (--r >= 0) n[r] = 0;
    for (r = Math.max(t - this.t, 0); r < e.t; ++r) n[this.t + r - t] = this.am(t - r, e[r], n, 0, 0, this.t + r - t);
    n.clamp(), n.drShiftTo(1, n)
}, ArcotJSBN.Barrett = function(e) {
    this.r2 = ArcotJSBN.nbi(), this.q3 = ArcotJSBN.nbi(), ArcotJSBN.BigInteger.ONE.dlShiftTo(2 * e.t, this.r2), this.mu = this.r2.divide(e), this.m = e
}, ArcotJSBN.barrettConvert = function(e) {
    if (e.s < 0 || e.t > 2 * this.m.t) return e.mod(this.m);
    if (e.compareTo(this.m) < 0) return e;
    var t = ArcotJSBN.nbi();
    return e.copyTo(t), this.reduce(t), t
}, ArcotJSBN.barrettRevert = function(e) {
    return e
}, ArcotJSBN.barrettReduce = function(e) {
    e.drShiftTo(this.m.t - 1, this.r2), e.t > this.m.t + 1 && (e.t = this.m.t + 1, e.clamp()), this.mu.multiplyUpperTo(this.r2, this.m.t + 1, this.q3), this.m.multiplyLowerTo(this.q3, this.m.t + 1, this.r2);
    while (e.compareTo(this.r2) < 0) e.dAddOffset(1, this.m.t + 1);
    e.subTo(this.r2, e);
    while (e.compareTo(this.m) >= 0) e.subTo(this.m, e)
}, ArcotJSBN.barrettSqrTo = function(e, t) {
    e.squareTo(t), this.reduce(t)
}, ArcotJSBN.barrettMulTo = function(e, t, n) {
    e.multiplyTo(t, n), this.reduce(n)
}, ArcotJSBN.Barrett.prototype.convert = ArcotJSBN.barrettConvert, ArcotJSBN.Barrett.prototype.revert = ArcotJSBN.barrettRevert, ArcotJSBN.Barrett.prototype.reduce = ArcotJSBN.barrettReduce, ArcotJSBN.Barrett.prototype.mulTo = ArcotJSBN.barrettMulTo, ArcotJSBN.Barrett.prototype.sqrTo = ArcotJSBN.barrettSqrTo, ArcotJSBN.bnModPow = function(e, t) {
    var n = e.bitLength(),
        r, i = ArcotJSBN.nbv(1),
        s;
    if (n <= 0) return i;
    n < 18 ? r = 1 : n < 48 ? r = 3 : n < 144 ? r = 4 : n < 768 ? r = 5 : r = 6, n < 8 ? s = new ArcotJSBN.Classic(t) : t.isEven() ? s = new ArcotJSBN.Barrett(t) : s = new ArcotJSBN.Montgomery(t);
    var o = new Array,
        u = 3,
        a = r - 1,
        f = (1 << r) - 1;
    o[1] = s.convert(this);
    if (r > 1) {
        var l = ArcotJSBN.nbi();
        s.sqrTo(o[1], l);
        while (u <= f) o[u] = ArcotJSBN.nbi(), s.mulTo(l, o[u - 2], o[u]), u += 2
    }
    var c = e.t - 1,
        h, p = !0,
        d = ArcotJSBN.nbi(),
        v;
    n = ArcotJSBN.nbits(e[c]) - 1;
    while (c >= 0) {
        n >= a ? h = e[c] >> n - a & f : (h = (e[c] & (1 << n + 1) - 1) << a - n, c > 0 && (h |= e[c - 1] >> this.DB + n - a)), u = r;
        while ((h & 1) == 0) h >>= 1, --u;
        (n -= u) < 0 && (n += this.DB, --c);
        if (p) o[h].copyTo(i), p = !1;
        else {
            while (u > 1) s.sqrTo(i, d), s.sqrTo(d, i), u -= 2;
            u > 0 ? s.sqrTo(i, d) : (v = i, i = d, d = v), s.mulTo(d, o[h], i)
        }
        while (c >= 0 && (e[c] & 1 << n) == 0) s.sqrTo(i, d), v = i, i = d, d = v, --n < 0 && (n = this.DB - 1, --c)
    }
    var m = s.revert(i);
    return m
}, ArcotJSBN.bnGCD = function(e) {
    var t = this.s < 0 ? this.negate() : this.clone(),
        n = e.s < 0 ? e.negate() : e.clone();
    if (t.compareTo(n) < 0) {
        var r = t;
        t = n, n = r
    }
    var i = t.getLowestSetBit(),
        s = n.getLowestSetBit();
    if (s < 0) return t;
    i < s && (s = i), s > 0 && (t.rShiftTo(s, t), n.rShiftTo(s, n));
    while (t.signum() > 0)(i = t.getLowestSetBit()) > 0 && t.rShiftTo(i, t), (i = n.getLowestSetBit()) > 0 && n.rShiftTo(i, n), t.compareTo(n) >= 0 ? (t.subTo(n, t), t.rShiftTo(1, t)) : (n.subTo(t, n), n.rShiftTo(1, n));
    return s > 0 && n.lShiftTo(s, n), n
}, ArcotJSBN.bnpModInt = function(e) {
    if (e <= 0) return 0;
    var t = this.DV % e,
        n = this.s < 0 ? e - 1 : 0;
    if (this.t > 0)
        if (t == 0) n = this[0] % e;
        else
            for (var r = this.t - 1; r >= 0; --r) n = (t * n + this[r]) % e;
    return n
}, ArcotJSBN.bnModInverse = function(e) {
    var t = e.isEven();
    if (this.isEven() && t || e.signum() == 0) return ArcotJSBN.BigInteger.ZERO;
    var n = e.clone(),
        r = this.clone(),
        i = ArcotJSBN.nbv(1),
        s = ArcotJSBN.nbv(0),
        o = ArcotJSBN.nbv(0),
        u = ArcotJSBN.nbv(1);
    while (n.signum() != 0) {
        while (n.isEven()) {
            n.rShiftTo(1, n);
            if (t) {
                if (!i.isEven() || !s.isEven()) i.addTo(this, i), s.subTo(e, s);
                i.rShiftTo(1, i)
            } else s.isEven() || s.subTo(e, s);
            s.rShiftTo(1, s)
        }
        while (r.isEven()) {
            r.rShiftTo(1, r);
            if (t) {
                if (!o.isEven() || !u.isEven()) o.addTo(this, o), u.subTo(e, u);
                o.rShiftTo(1, o)
            } else u.isEven() || u.subTo(e, u);
            u.rShiftTo(1, u)
        }
        n.compareTo(r) >= 0 ? (n.subTo(r, n), t && i.subTo(o, i), s.subTo(u, s)) : (r.subTo(n, r), t && o.subTo(i, o), u.subTo(s, u))
    }
    return r.compareTo(ArcotJSBN.BigInteger.ONE) != 0 ? ArcotJSBN.BigInteger.ZERO : u.compareTo(e) >= 0 ? u.subtract(e) : u.signum() < 0 ? (u.addTo(e, u), u.signum() < 0 ? u.add(e) : u) : u
}, ArcotJSBN.lowprimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509], ArcotJSBN.lplim = (1 << 26) / ArcotJSBN.lowprimes[ArcotJSBN.lowprimes.length - 1], ArcotJSBN.bnIsProbablePrime = function(e) {
    var t, n = this.abs();
    if (n.t == 1 && n[0] <= ArcotJSBN.lowprimes[ArcotJSBN.lowprimes.length - 1]) {
        for (t = 0; t < ArcotJSBN.lowprimes.length; ++t)
            if (n[0] == ArcotJSBN.lowprimes[t]) return !0;
        return !1
    }
    if (n.isEven()) return !1;
    t = 1;
    while (t < ArcotJSBN.lowprimes.length) {
        var r = ArcotJSBN.lowprimes[t],
            i = t + 1;
        while (i < ArcotJSBN.lowprimes.length && r < ArcotJSBN.lplim) r *= ArcotJSBN.lowprimes[i++];
        r = n.modInt(r);
        while (t < i)
            if (r % ArcotJSBN.lowprimes[t++] == 0) return !1
    }
    return n.millerRabin(e)
}, ArcotJSBN.bnpMillerRabin = function(e) {
    var t = this.subtract(ArcotJSBN.BigInteger.ONE),
        n = t.getLowestSetBit();
    if (n <= 0) return !1;
    var r = t.shiftRight(n);
    e = e + 1 >> 1, e > ArcotJSBN.lowprimes.length && (e = ArcotJSBN.lowprimes.length);
    var i = ArcotJSBN.nbi();
    for (var s = 0; s < e; ++s) {
        i.fromInt(ArcotJSBN.lowprimes[s]);
        var o = i.modPow(r, this);
        if (o.compareTo(ArcotJSBN.BigInteger.ONE) != 0 && o.compareTo(t) != 0) {
            var u = 1;
            while (u++ < n && o.compareTo(t) != 0) {
                o = o.modPowInt(2, this);
                if (o.compareTo(ArcotJSBN.BigInteger.ONE) == 0) return !1
            }
            if (o.compareTo(t) != 0) return !1
        }
    }
    return !0
}, ArcotJSBN.BigInteger.prototype.chunkSize = ArcotJSBN.bnpChunkSize, ArcotJSBN.BigInteger.prototype.toRadix = ArcotJSBN.bnpToRadix, ArcotJSBN.BigInteger.prototype.fromRadix = ArcotJSBN.bnpFromRadix, ArcotJSBN.BigInteger.prototype.fromNumber = ArcotJSBN.bnpFromNumber, ArcotJSBN.BigInteger.prototype.bitwiseTo = ArcotJSBN.bnpBitwiseTo, ArcotJSBN.BigInteger.prototype.changeBit = ArcotJSBN.bnpChangeBit, ArcotJSBN.BigInteger.prototype.addTo = ArcotJSBN.bnpAddTo, ArcotJSBN.BigInteger.prototype.dMultiply = ArcotJSBN.bnpDMultiply, ArcotJSBN.BigInteger.prototype.dAddOffset = ArcotJSBN.bnpDAddOffset, ArcotJSBN.BigInteger.prototype.multiplyLowerTo = ArcotJSBN.bnpMultiplyLowerTo, ArcotJSBN.BigInteger.prototype.multiplyUpperTo = ArcotJSBN.bnpMultiplyUpperTo, ArcotJSBN.BigInteger.prototype.modInt = ArcotJSBN.bnpModInt, ArcotJSBN.BigInteger.prototype.millerRabin = ArcotJSBN.bnpMillerRabin, ArcotJSBN.BigInteger.prototype.clone = ArcotJSBN.bnClone, ArcotJSBN.BigInteger.prototype.intValue = ArcotJSBN.bnIntValue, ArcotJSBN.BigInteger.prototype.byteValue = ArcotJSBN.bnByteValue, ArcotJSBN.BigInteger.prototype.shortValue = ArcotJSBN.bnShortValue, ArcotJSBN.BigInteger.prototype.signum = ArcotJSBN.bnSigNum, ArcotJSBN.BigInteger.prototype.toByteArray = ArcotJSBN.bnToByteArray, ArcotJSBN.BigInteger.prototype.equals = ArcotJSBN.bnEquals, ArcotJSBN.BigInteger.prototype.min = ArcotJSBN.bnMin, ArcotJSBN.BigInteger.prototype.max = ArcotJSBN.bnMax, ArcotJSBN.BigInteger.prototype.and = ArcotJSBN.bnAnd, ArcotJSBN.BigInteger.prototype.or = ArcotJSBN.bnOr, ArcotJSBN.BigInteger.prototype.xor = ArcotJSBN.bnXor, ArcotJSBN.BigInteger.prototype.andNot = ArcotJSBN.bnAndNot, ArcotJSBN.BigInteger.prototype.not = ArcotJSBN.bnNot, ArcotJSBN.BigInteger.prototype.shiftLeft = ArcotJSBN.bnShiftLeft, ArcotJSBN.BigInteger.prototype.shiftRight = ArcotJSBN.bnShiftRight, ArcotJSBN.BigInteger.prototype.getLowestSetBit = ArcotJSBN.bnGetLowestSetBit, ArcotJSBN.BigInteger.prototype.bitCount = ArcotJSBN.bnBitCount, ArcotJSBN.BigInteger.prototype.testBit = ArcotJSBN.bnTestBit, ArcotJSBN.BigInteger.prototype.setBit = ArcotJSBN.bnSetBit, ArcotJSBN.BigInteger.prototype.clearBit = ArcotJSBN.bnClearBit, ArcotJSBN.BigInteger.prototype.flipBit = ArcotJSBN.bnFlipBit, ArcotJSBN.BigInteger.prototype.add = ArcotJSBN.bnAdd, ArcotJSBN.BigInteger.prototype.subtract = ArcotJSBN.bnSubtract, ArcotJSBN.BigInteger.prototype.multiply = ArcotJSBN.bnMultiply, ArcotJSBN.BigInteger.prototype.divide = ArcotJSBN.bnDivide, ArcotJSBN.BigInteger.prototype.remainder = ArcotJSBN.bnRemainder, ArcotJSBN.BigInteger.prototype.divideAndRemainder = ArcotJSBN.bnDivideAndRemainder, ArcotJSBN.BigInteger.prototype.modPow = ArcotJSBN.bnModPow, ArcotJSBN.BigInteger.prototype.modInverse = ArcotJSBN.bnModInverse, ArcotJSBN.BigInteger.prototype.pow = ArcotJSBN.bnPow, ArcotJSBN.BigInteger.prototype.gcd = ArcotJSBN.bnGCD, ArcotJSBN.BigInteger.prototype.isProbablePrime = ArcotJSBN.bnIsProbablePrime;
var ArcotPRNG = {};
ArcotPRNG.Arcfour = function() {
    this.i = 0, this.j = 0, this.S = new Array
}, ArcotPRNG.ARC4init = function(e) {
    var t, n, r;
    for (t = 0; t < 256; ++t) this.S[t] = t;
    n = 0;
    for (t = 0; t < 256; ++t) n = n + this.S[t] + e[t % e.length] & 255, r = this.S[t], this.S[t] = this.S[n], this.S[n] = r;
    this.i = 0, this.j = 0
}, ArcotPRNG.ARC4next = function() {
    var e;
    return this.i = this.i + 1 & 255, this.j = this.j + this.S[this.i] & 255, e = this.S[this.i], this.S[this.i] = this.S[this.j], this.S[this.j] = e, this.S[e + this.S[this.i] & 255]
}, ArcotPRNG.Arcfour.prototype.init = ArcotPRNG.ARC4init, ArcotPRNG.Arcfour.prototype.next = ArcotPRNG.ARC4next, ArcotPRNG.prng_newstate = function() {
    return new ArcotPRNG.Arcfour
}, ArcotPRNG.rng_psize = 256, ArcotPRNG.rng_state, ArcotPRNG.rng_pool, ArcotPRNG.rng_pptr, ArcotPRNG.rng_seed_int = function(e) {
    ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] ^= e & 255, ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] ^= e >> 8 & 255, ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] ^= e >> 16 & 255, ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] ^= e >> 24 & 255, ArcotPRNG.rng_pptr >= ArcotPRNG.rng_psize && (ArcotPRNG.rng_pptr -= ArcotPRNG.rng_psize)
}, ArcotPRNG.rng_seed_time = function() {
    ArcotPRNG.rng_seed_int((new Date).getTime())
};
if (ArcotPRNG.rng_pool == null) {
    ArcotPRNG.rng_pool = new Array, ArcotPRNG.rng_pptr = 0, ArcotPRNG.t;
    if (navigator.appName == "Netscape" && navigator.appVersion < "5" && window.crypto) {
        ArcotPRNG.z = window.crypto.random(32);
        for (ArcotPRNG.t = 0; ArcotPRNG.t < ArcotPRNG.z.length; ++ArcotPRNG.t) ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] = ArcotPRNG.z.charCodeAt(ArcotPRNG.t) & 255
    }
    while (ArcotPRNG.rng_pptr < ArcotPRNG.rng_psize) ArcotPRNG.t = Math.floor(65536 * Math.random()), ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] = ArcotPRNG.t >>> 8, ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr++] = ArcotPRNG.t & 255;
    ArcotPRNG.rng_pptr = 0, ArcotPRNG.rng_seed_time()
}
ArcotPRNG.rng_get_byte = function() {
    if (ArcotPRNG.rng_state == null) {
        ArcotPRNG.rng_seed_time(), ArcotPRNG.rng_state = ArcotPRNG.prng_newstate(), ArcotPRNG.rng_state.init(ArcotPRNG.rng_pool);
        for (ArcotPRNG.rng_pptr = 0; ArcotPRNG.rng_pptr < ArcotPRNG.rng_pool.length; ++ArcotPRNG.rng_pptr) ArcotPRNG.rng_pool[ArcotPRNG.rng_pptr] = 0;
        ArcotPRNG.rng_pptr = 0
    }
    return ArcotPRNG.rng_state.next()
}, ArcotPRNG.rng_get_bytes = function(e) {
    var t;
    for (t = 0; t < e.length; ++t) e[t] = ArcotPRNG.rng_get_byte()
}, ArcotPRNG.SecureRandom = function() {}, ArcotPRNG.SecureRandom.prototype.nextBytes = ArcotPRNG.rng_get_bytes;
var ArcotRSA = {};
ArcotRSA.parseBigInt = function(e, t) {
    return new ArcotJSBN.BigInteger(e, t)
}, ArcotRSA.linebrk = function(e, t) {
    var n = "",
        r = 0;
    while (r + t < e.length) n += e.substring(r, r + t) + "\n", r += t;
    return n + e.substring(r, e.length)
}, ArcotRSA.byte2Hex = function(e) {
    return e < 16 ? "0" + e.toString(16) : e.toString(16)
}, ArcotRSA.pkcs1pad2 = function(e, t) {
    if (t < e.length + 11) return alert("Message too long for RSA"), null;
    var n = new Array,
        r = e.length - 1;
    while (r >= 0 && t > 0) n[--t] = e.charCodeAt(r--);
    n[--t] = 0;
    var i = new SecureRandom,
        s = new Array;
    while (t > 2) {
        s[0] = 0;
        while (s[0] == 0) i.nextBytes(s);
        n[--t] = s[0]
    }
    return n[--t] = 2, n[--t] = 0, new ArcotJSBN.BigInteger(n)
}, ArcotRSA.RSAKey = function() {
    this.n = null, this.e = 0, this.d = null, this.p = null, this.q = null, this.dmp1 = null, this.dmq1 = null, this.coeff = null
}, ArcotRSA.RSASetPublic = function(e, t) {
    e != null && t != null && e.length > 0 && t.length > 0 ? (this.n = ArcotRSA.parseBigInt(e, 16), this.e = parseInt(t, 16)) : alert("Invalid RSA public key")
}, ArcotRSA.RSADoPublic = function(e) {
    return e.modPowInt(this.e, this.n)
}, ArcotRSA.RSAEncrypt = function(e) {
    var t = ArcotRSA.pkcs1pad2(e, this.n.bitLength() + 7 >> 3);
    if (t == null) return null;
    var n = this.doPublic(t);
    if (n == null) return null;
    var r = n.toString(16);
    return (r.length & 1) == 0 ? r : "0" + r
}, ArcotRSA.RSAKey.prototype.doPublic = ArcotRSA.RSADoPublic, ArcotRSA.RSAKey.prototype.setPublic = ArcotRSA.RSASetPublic, ArcotRSA.RSAKey.prototype.encrypt = ArcotRSA.RSAEncrypt, ArcotRSA.pkcs1unpad2 = function(e, t) {
    var n = e.toByteArray(),
        r = 0;
    while (r < n.length && n[r] == 0) ++r;
    if (n.length - r != t - 1 || n[r] != 2) return null;
    ++r;
    while (n[r] != 0)
        if (++r >= n.length) return null;
    var i = "";
    while (++r < n.length) i += String.fromCharCode(n[r]);
    return i
}, ArcotRSA.RSASetPrivate = function(e, t, n) {
    e != null && t != null && e.length > 0 && t.length > 0 ? (this.n = ArcotRSA.parseBigInt(e, 16), this.e = parseInt(t, 16), this.d = ArcotRSA.parseBigInt(n, 16)) : alert("Invalid RSA private key")
}, ArcotRSA.RSASetPrivateEx = function(e, t, n, r, i, s, o, u) {
    e != null && t != null && e.length > 0 && t.length > 0 ? (this.n = ArcotRSA.parseBigInt(e, 16), this.e = parseInt(t, 16), this.d = ArcotRSA.parseBigInt(n, 16), this.p = ArcotRSA.parseBigInt(r, 16), this.q = ArcotRSA.parseBigInt(i, 16), this.dmp1 = ArcotRSA.parseBigInt(s, 16), this.dmq1 = ArcotRSA.parseBigInt(o, 16), this.coeff = ArcotRSA.parseBigInt(u, 16)) : alert("Invalid RSA private key")
}, ArcotRSA.RSAGenerate = function(e, t) {
    var n = new SecureRandom,
        r = e >> 1;
    this.e = parseInt(t, 16);
    var i = new ArcotJSBN.BigInteger(t, 16);
    for (;;) {
        for (;;) {
            this.p = new ArcotJSBN.BigInteger(e - r, 1, n);
            if (this.p.subtract(ArcotJSBN.BigInteger.ONE).gcd(i).compareTo(ArcotJSBN.BigInteger.ONE) == 0 && this.p.isProbablePrime(10)) break
        }
        for (;;) {
            this.q = new ArcotJSBN.BigInteger(r, 1, n);
            if (this.q.subtract(ArcotJSBN.BigInteger.ONE).gcd(i).compareTo(ArcotJSBN.BigInteger.ONE) == 0 && this.q.isProbablePrime(10)) break
        }
        if (this.p.compareTo(this.q) <= 0) {
            var s = this.p;
            this.p = this.q, this.q = s
        }
        var o = this.p.subtract(ArcotJSBN.BigInteger.ONE),
            u = this.q.subtract(ArcotJSBN.BigInteger.ONE),
            a = o.multiply(u);
        if (a.gcd(i).compareTo(ArcotJSBN.BigInteger.ONE) == 0) {
            this.n = this.p.multiply(this.q), this.d = i.modInverse(a), this.dmp1 = this.d.mod(o), this.dmq1 = this.d.mod(u), this.coeff = this.q.modInverse(this.p);
            break
        }
    }
}, ArcotRSA.RSADoPrivate = function(e) {
    if (this.p == null || this.q == null) return e.modPow(this.d, this.n);
    var t = e.mod(this.p).modPow(this.dmp1, this.p),
        n = e.mod(this.q).modPow(this.dmq1, this.q);
    while (t.compareTo(n) < 0) t = t.add(this.p);
    return t.subtract(n).multiply(this.coeff).mod(this.p).multiply(this.q).add(n)
}, ArcotRSA.RSADecrypt = function(e) {
    var t = ArcotRSA.parseBigInt(e, 16),
        n = this.doPrivate(t);
    return n == null ? null : ArcotRSA.pkcs1unpad2(n, this.n.bitLength() + 7 >> 3)
}, ArcotRSA.RSAKey.prototype.doPrivate = ArcotRSA.RSADoPrivate, ArcotRSA.RSAKey.prototype.setPrivate = ArcotRSA.RSASetPrivate, ArcotRSA.RSAKey.prototype.setPrivateEx = ArcotRSA.RSASetPrivateEx, ArcotRSA.RSAKey.prototype.generate = ArcotRSA.RSAGenerate, ArcotRSA.RSAKey.prototype.decrypt = ArcotRSA.RSADecrypt, DevLockerCurrent.prototype.doDevLockMigration = function(e) {
    if (e.getType() == "plugin") {
        var t = {},
            n = e.GetAll(),
            r = n.length;
        for (var i = 0; i < r; i++) try {
            var s = n[i],
                o = WalletUtil.parse(e.serialize(s)),
                u = s.dltu;
            if (u) {
                var a = u.split(":");
                if (a.length == 1) {
                    ArcotLogger.log("DevLockerCurrent::doDevLockMigration found V2 records");
                    var f = a[0],
                        l = 2;
                    if (l == this.dlVersion) continue;
                    if (u.toLowerCase() == StoreImplPlugin.DLTU_NONE) s.dltu = this.dlVersion.toString() + ":" + StoreImplPlugin.DLTU_NONE;
                    else {
                        var c = this.getKeyForVersion(f, l);
                        this.recamoWithCurrentDLKey(o, e, s, c)
                    }
                } else if (a.length == 2) {
                    var h = a[0],
                        l = parseInt(h);
                    if (l == this.dlVersion) continue;
                    if (l < this.dlVersion) {
                        var f = a[1];
                        if (f.toLowerCase() == StoreImplPlugin.DLTU_NONE) s.dltu = this.dlVersion.toString() + ":" + StoreImplPlugin.DLTU_NONE;
                        else {
                            var c = this.getKeyForVersion(f, l);
                            this.recamoWithCurrentDLKey(o, e, s, c)
                        }
                    }
                }
            } else {
                var l = 1,
                    p = this.getKeyForVersion(null, l);
                this.recamoWithCurrentDLKey(o, e, s, p)
            }
            var d = e.Save(s, t);
            if (d != ArcotErrorCodes.ERR_NONE) {
                ArcotLogger.log("Migration Error code is:" + d);
                continue
            }
        } catch (v) {
            ArcotLogger.log("Exception in doDevLockMigration:" + v);
            continue
        }
    }
    return
}, DevLockerCurrent.prototype.recamoWithCurrentDLKey = function(e, t, n, r) {
    var i = t.unlock(e, n, r),
        s = ArcotAIDMobile.bytear2hexstr(i),
        o = e.arcotcards[0].arcotPrivateKey.camouflagedKey;
    t.replaceKey(n, o, s), t.deviceLock(n, t.getType())
}, DevLockerCurrent.prototype.isValid = function() {
    return this.dlVersion >= 3 ? !0 : !1
}, DevLockerCurrent.prototype.GetVersion = function() {
    return this.dlVersion
}, DevLockerCurrent.prototype.getKey = function(t, n, r, i) {
    try {
        var s = (new AuthMinderPlugin).getPlugin();
        if (!s) return null;
        if (!(!t || t.toUpperCase() != "FALSE" && t.toUpperCase() != "NO")) return i.dltu = this.dlVersion.toString() + ":" + StoreImplPlugin.DLTU_NONE, null;
        var o = {
            DNA: null
        };
        if (r) this.dlVersion == 3 && (n != undefined && n != null ? o.inputAttributes = n : o.inputAttributes = "");
        else {
            if (n == undefined || n == null) return null;
            var u = n.split(":");
            u.size == 2 && (o.inputAttributes = u[1])
        }
        var a = s.GetDeviceDNA(o);
        if (a == AuthMinderPlugin.E_SUCCESS) {
            var f = o.DNA;
            return i.dltu = this.dlVersion.toString() + ":" + o.outputAttributes, ArcotLogger.log("DeviceLock: done"), f
        }
    } catch (l) {
        ArcotLogger.log("Exception while getting DeviceLock: " + l)
    }
    return null
}, DevLockerCurrent.prototype.getKeyForVersion = function(e, t) {
    try {
        var n = (new AuthMinderPlugin).getPlugin();
        if (!n) return null;
        var r = {
            version: t.toString(),
            DNA: null
        };
        t > 1 && t < this.dlVersion && e && (r.inputAttributes = e);
        var i = n.GetDeviceDNA(r);
        if (i == AuthMinderPlugin.E_SUCCESS) {
            var s = r.DNA;
            return s
        }
    } catch (o) {}
    return null
}, StoreAID.PLUGIN_DLTUSTR = "_dltu_", StoreAID.prototype = new StoreBase({
    inheriting: !0
}), StoreAID.base = StoreBase.prototype, StoreAID.prototype.GetAll = function() {
    var e = {},
        t = [];
    StoreAID.base.loadAll.call(this, e);
    var n = e.values,
        r = e.keys,
        i = e.dltus;
    for (var s = 0; s < n.length; s++) {
        var o = WalletUtil.parse(this.serialize(n[s])),
            u = this.getKey(o);
        if (u == "arcot.wallet.memory" && this.impl.getType() == "cookie") continue;
        if (u == "arcot.wallet.0" && this.impl.getType() == "memory") continue;
        var a = n[s];
        i != undefined && i != null && (a.dltu = i[s]), t.push(a)
    }
    return t
}, StoreAID.prototype.Get = function(e, t) {
    e = e.toUpperCase(), t != null && (t = t.toUpperCase());
    var n = this.GetAll();
    for (var r = 0; r < n.length; r++) {
        var i = this.serialize(n[r]),
            s = WalletUtil.parse(i);
        s.dltu = n[r].dltu;
        if (e == s.walletname.toUpperCase()) {
            if (t == null || t.length == 0) return s;
            if (t == s.arcotcards[0].orgname.toUpperCase()) return s
        }
    }
    return null
}, StoreAID.prototype.Save = function(e, t) {
    var n = WalletUtil.parse(e.b64EncodedArcotID),
        r = this.getKey(n);
    if (t == undefined || t == null) t = {};
    t.inputAttributes = e.dltu;
    var i = StoreAID.base.save.call(this, r, e, t);
    return i ? ArcotErrorCodes.ERR_NONE : ArcotErrorCodes.ERR_UNKNOWN
}, StoreAID.prototype.getKey = function(e) {
    var t;
    return this.impl instanceof StoreImplCookies ? t = "arcot.wallet.0" : this.impl instanceof StoreImplMemory ? t = "arcot.wallet.memory" : t = "aid_" + WalletUtil.getItemNameFromWallet(e), t
}, StoreAID.prototype.GetWalletCount = function() {
    var e = this.GetAll();
    return e.length
}, StoreAID.prototype.GetWalletAt = function(e) {
    var t = this.GetAll();
    if (e >= t.length) return null;
    var n = t[e].b64EncodedArcotID,
        r = WalletUtil.parse(n);
    return r.dltu = t[e].dltu, r
}, StoreAID.prototype.Delete = function(e, t) {
    var n = this.Get(e, t);
    if (n) {
        var r = this.getKey(n);
        return StoreAID.base.remove.call(this, r)
    }
    return !1
}, StoreAID.prototype.DeleteByAlias = function(e, t, n) {
    var r = this.GetAll();
    for (var i = 0; i < r.length; i++) {
        var s = r[i],
            o = WalletUtil.parse(s),
            u = "Alias." + n,
            a = o.arcotcards[0].namedAttributes[u];
        a != null && (a = ArcotUtil.util_hex2str(a));
        if (a != null && a.toUpperCase() == e.toUpperCase()) {
            if (t == null || t.length == 0) {
                var f = this.getKey(o);
                return StoreAID.base.remove.call(this, f)
            }
            if (t.toUpperCase() == o.arcotcards[0].orgname.toUpperCase()) {
                var f = this.getKey(o);
                return StoreAID.base.remove.call(this, f)
            }
        }
    }
    return !1
}, StoreAID.prototype.DeleteAll = function() {
    var e = this.GetAll();
    for (var t = 0; t < e.length; t++) {
        var n = e[t],
            r = WalletUtil.parse(n),
            i = this.getKey(r);
        StoreAID.base.remove.call(this, i)
    }
}, StoreAID.prototype.serialize = function(e) {
    return e.b64EncodedArcotID
}, StoreAID.prototype.deserialize = function(e) {
    var t = Object();
    return t.b64EncodedArcotID = e, t.dltu = "", t.key = "", t
}, StoreAID.prototype.isValidEntry = function(e, t) {
    return e.substr(0, 12) == "arcot.wallet" || e.substr(0, 4) == "aid_"
}, StoreAID.prototype.getDevLockKey = function(e, t, n) {
    if (t != "plugin" || this.devicelocker == null) return null;
    var r, i = new Object,
        s;
    s = e.arcotcards[0].namedAttributes[StoreImplPlugin.DLR], s != undefined && s != null && (s = ArcotUtil.util_hex2str(s));
    if (n) {
        var o = e.arcotcards[0].namedAttributes[StoreImplPlugin.DLT];
        o != undefined && o != null && (o = ArcotUtil.util_hex2str(o)), r = this.devicelocker.getKey(s, o, !0, i), e.dltu = i.dltu
    } else {
        if (e.dltu == StoreImplPlugin.DLTU_NONE) return null;
        r = this.devicelocker.getKey(s, e.dltu, !1, i), e.dltu = i.dltu
    }
    return r == null ? r : (r = r.trim(), r.length == 0 ? null : r)
}, StoreAID.prototype.deviceLock = function(e, t) {
    if (t != "plugin" || this.devicelocker == null) return !1;
    if (!e.hasOwnProperty("b64EncodedArcotID")) return !1;
    var n = WalletUtil.parse(e.b64EncodedArcotID);
    if (n == null) return !1;
    var r = this.getDevLockKey(n, this.getType(), !0);
    return e.dltu = n.dltu, r == null ? !1 : (r = r.trim(), r.length == 0 ? !1 : this.lock(n, e, r))
}, StoreAID.prototype.lock = function(e, t, n) {
    var r = new ArcotAIDMobile.ArcotDigestRep(e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageType, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.salt, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.iterationCount, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.checksum, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.start, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.span),
        i = ArcotAIDMobile.hexstr2bytes(e.arcotcards[0].arcotPrivateKey.camouflagedKey),
        s = r.applyPinToKey(n, i),
        o = ArcotAIDMobile.bytear2hexstr(s),
        u = e.arcotcards[0].arcotPrivateKey.camouflagedKey;
    return this.replaceKey(t, u, o)
}, StoreAID.prototype.replaceKey = function(e, t, n) {
    if (n && n.length == t.length) {
        var r = ArcotBase64.b64tohex(e.b64EncodedArcotID);
        return r = r.replace(t, n), e.b64EncodedArcotID = ArcotBase64.hex2b64(r), !0
    }
    return !1
}, StoreAID.prototype.deviceUnlock = function(e, t, n, r) {
    if (t != "plugin" || this.devicelocker == null) return !1;
    r == undefined && e.hasOwnProperty("b64EncodedArcotID") && (r = WalletUtil.parse(e.b64EncodedArcotID));
    var i = this.getDevLockKey(r, this.getType(), !1);
    if (i == null) return !1;
    i = i.trim();
    if (i.length == 0) return !1;
    var s = this.unlock(r, e, i),
        o = new ArcotAIDMobile.ArcotDigestRep(r.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageType, r.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.salt, r.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.iterationCount, r.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.checksum, r.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.start, r.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.span);
    return e.decamkey = o.decrypt(n, s), !0
}, StoreAID.prototype.unlock = function(e, t, n) {
    var r = ArcotAIDMobile.hexstr2bytes(e.arcotcards[0].arcotPrivateKey.camouflagedKey),
        i = new ArcotAIDMobile.ArcotDigestRep(e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageType, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.salt, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.iterationCount, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.checksum, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.start, e.arcotcards[0].arcotPrivateKey.arcotDigest.camouflageParameters.span),
        s = i.applyPinToKey(n, r);
    return s
}, StoreAID.prototype.doDevLockMigration = function() {
    if (this.getType() != "plugin" || this.devicelocker == null) return;
    this.devicelocker.doDevLockMigration(this)
}, StoreAID.prototype.doDevLockMigrationV2 = function() {
    if (this.getType() == "plugin") {
        var e = {},
            t = this.GetAll(),
            n = t.length;
        for (var r = 0; r < n; r++) try {
            var i = t[r],
                s = WalletUtil.parse(this.serialize(i)),
                o = i.dltu;
            if (!o) {
                var u = new DeviceLock(this.obj),
                    a = u.getKey(null, !1, null),
                    f = this.unlock(s, i, a),
                    l = ArcotAIDMobile.bytear2hexstr(f),
                    c = s.arcotcards[0].arcotPrivateKey.camouflagedKey;
                this.replaceKey(i, c, l), this.deviceLock(i, this.getType());
                var h = this.Save(i, e);
                h != ArcotErrorCodes.ERR_NONE && ArcotLogger.log("Migration Error code is:" + h)
            }
        } catch (p) {
            ArcotLogger.log("Exception in doDevLockMigrationV2:" + p);
            continue
        }
    }
    return
}, StoreAIDMemoryStorage.prototype = new StoreAID({
    inheriting: !0
});
var ArcotLogger = {};
ArcotLogger.debug = !1, ArcotLogger.log = function(e) {
    ArcotLogger.debug && alert(e)
};
var ArcotUtil = {
    util_hex2str: function(e) {
        var t = 0,
            n = "";
        while (t < e.length) n += "%" + e.substr(t, 2), t += 2;
        return decodeURIComponent(n)
    },
    util_hex2tstring: function(e) {
        if (e.length % 2 != 0) return "";
        var t, n, r = "";
        for (t = 0; t < e.length; t += 2) n = parseInt(e.substring(t, t + 2), 16), r += String.fromCharCode(n);
        return r
    },
    util_tstring2hex: function(e) {
        var t = "",
            n;
        for (n = 0; n < e.length; n++) t += ArcotUtil.util_byte2hex(e.charCodeAt(n));
        return t
    },
    util_hex2bytes: function(e) {
        if (e.length % 2 != 0) return "";
        var t, n, r = new Array;
        for (t = 0; t < e.length; t += 2) n = parseInt(e.substring(t, t + 2), 16), r[t / 2] = String.fromCharCode(n);
        return r
    },
    util_hex2bytearray: function(e) {
        if (e.length % 2 != 0) return "";
        var t, n, r = new Array;
        for (t = 0; t < e.length; t += 2) r[t / 2] = parseInt(e.substring(t, t + 2), 16);
        return r
    },
    util_byte2hex: function(e) {
        var t = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"),
            n = e >> 4 & 15,
            r = e & 15,
            i = t[n] + t[r];
        return i
    },
    util_bytes2tstring: function(e) {
        var t, n;
        for (n = 0; n < e.length; n++) {
            var r = e[n];
            t += String.fromCharCode(r)
        }
        return
    },
    flip: function(e) {
        var t = "f0F0 e1E1 d2D2 c3C3 b4B4 a5A5 696 787";
        return t.charAt(t.indexOf(e) + 1)
    },
    util_flipBits: function(e) {
        var t = "";
        for (var n = 0; n < e.length; n++) t += ArcotUtil.flip(e.charAt(n));
        return t
    }
};
WalletUtil = new Object, WalletUtil.parse = function(e) {
    var t = ArcotBase64.b64tohex(e),
        n = new ArcotAIDMobile.ArcotWallet,
        r = ArcotASN1JS.decode(t, n);
    return n
}, WalletUtil.getItemNameFromWallet = function(e) {
    var t = e.walletname,
        n = e.arcotcards[0].orgname,
        r = window.location.hostname;
    ArcotAIDMobile.debug && alert("Walletname is :" + t), ArcotAIDMobile.debug && alert("Org name is:" + n), ArcotAIDMobile.debug && alert("Domain name is:" + r);
    var i = t + n + r;
    i = unescape(encodeURIComponent(i));
    var s = ArcotSHA1.hex_sha1(i.toUpperCase());
    return ArcotAIDMobile.debug && alert(s), s
}, WalletUtil.getFileName = function(e, t) {
    var n = window.location.hostname,
        r = e + t + n;
    r = unescape(encodeURIComponent(r));
    var i = ArcotSHA1.hex_sha1(r.toUpperCase());
    return ArcotAIDMobile.debug && alert(i), i
}, WalletUtil.getCardAttribute = function(e, t) {
    return e.arcotcards[0].namedAttributes[t]
};
try {
    typeof arcotClientReady_ != "undefined" && arcotClientReady_()
} catch (e) {}
try {
    typeof lt_arcotClientReady_ != "undefined" && lt_arcotClientReady_()
} catch (e) {};