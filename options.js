var subTasks = [{
    summary: "Research",
}, {
    summary: "Recreate Defect",
}, {
    summary: "Fix Defect",
}, {
    summary: "Code Review / PR",
}, {
    summary: "Automated Test Passed",
}, {
    summary: "Fortify Acceptance",
}, {
    summary: "Test Plan",
}, {
    summary: "QA Acceptance",
}, {
    summary: "Document Results",
}, {
    summary: "PO Acceptance",
}, {
    summary: "Release Notes",
}];

document.addEventListener('DOMContentLoaded', function() {
    var createButton = document.getElementById('createButton');
    createButton.addEventListener('click', function() {
        createSubTask();
    });

    loadSubTasks();
});

function loadSubTasks() {
    for (prop in subTasks) {
        var divElement = document.createElement("div");
        divElement.className = "subTaskContainer";

        var inputElemnt = document.createElement("input");
        inputElemnt.type = "checkbox";
        inputElemnt.className = "subtasks";
        inputElemnt.value = subTasks[prop].summary;
        inputElemnt.id = subTasks[prop].summary;

        var labelElement = document.createElement("label");
        labelElement.htmlFor = subTasks[prop].summary;
        labelElement.innerText = subTasks[prop].summary;

        divElement.appendChild(inputElemnt);
        divElement.appendChild(labelElement);
        document.getElementById('container').appendChild(divElement);
    }
};

function createSubTask() {
    var checkedValues = $('.subtasks:checked').map(function() {
        return this.value;
    }).get();

    var subTasksCollection = [];
    checkedValues.forEach(function(selectedTask) {
        subTasks.forEach(function(subTask) {
            if (subTask.summary === selectedTask) {
                subTasksCollection.push(subTask);
            }
        })
    }, this);

    $('.subtasks:checked').prop('checked', false);

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, subTasksCollection, function(response) {
            console.log(response.farewell);
        });
    });

}